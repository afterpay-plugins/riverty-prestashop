<?php

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

if (!class_exists('Afterpay\Afterpay')) {
    $autoloadLocation = dirname(__FILE__) . '/vendor/autoload.php';
    if (file_exists($autoloadLocation)) {
        require_once $autoloadLocation;
    }
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class Riverty extends PaymentModule
{
    protected $config_form = false;
    private $paymentMethods;
    private $afterpayPaymentMethods;
    private $afterpayCurrencyRestrictions;

    public function __construct()
    {
        $this->name = 'riverty';
        $this->tab = 'payments_gateways';
        $this->version = '1.9.0';
        $this->author = 'Riverty';
        $this->need_instance = 1;
        $this->module_key = 'fa3117c949a6827c34a57a9964ef5e56';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = 'Riverty';
        $this->description =
            'The official Prestashop module for the payment methods of Riverty. Riverty is the new AfterPay.';

        $this->confirmUninstall = 'Are you sure you want to uninstall the Riverty module?';

        $this->limited_countries = array('NL', 'BE', 'DE', 'AT', 'CH', 'NO', 'SE', 'FI', 'DK');

        $this->limited_currencies = array('EUR', 'SEK', 'DKK', 'NOK', 'CHF');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        $this->afterpayPaymentMethods = [
            'nl' => [
                'digitalinvoice',
                'directdebit',
                'payin3',
                'business'
            ],
            'be' => [
                'digitalinvoice'
            ],
            'de' => [
                'digitalinvoice',
                'directdebit',
                'installments'
            ],
            'at' => [
                'digitalinvoice',
                'directdebit',
                'installments'
            ],
            'ch' => [
                'digitalinvoice'
            ],
            'no' => [
                'digitalinvoice'
            ],
            'se' => [
                'digitalinvoice'
            ],
            'fi' => [
                'digitalinvoice'
            ],
            'dk' => [
                'digitalinvoice'
            ],
        ];

        $this->afterpayCurrencyRestrictions = [
            'EUR' => [
                'nl',
                'be',
                'de',
                'at',
                'fi',
            ],
            'SEK' => [
                'se'
            ],
            'DKK' => [
                'dk'
            ],
            'NOK' => [
                'no'
            ],
            'CHF' => [
                'ch'
            ]
        ];

        // Add refund hook
        $this->registerHook('actionProductCancel');
        $this->registerHook('actionOrderStatusUpdate');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = 'You have to enable the cURL extension on your server to install this module';
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false) {
            $this->_errors[] = 'This module is not available in your country';
            return false;
        }

        Configuration::updateValue('AFTERPAY_ENABLED', false);

        // Check if AfterPay pending status exists.
        if (
            !($this->getOrderStateId('AfterPay - Awaiting payment')) &&
            !($this->getOrderStateId('AfterPay - Authorized')) &&
            !($this->getOrderStateId('AfterPay - Rejected')) &&
            !($this->getOrderStateId('AfterPay - Validation issue')) &&
            !($this->getOrderStateId('AfterPay - Captured')) &&
            !($this->getOrderStateId('AfterPay - Partially refunded')) &&
            !($this->getOrderStateId('AfterPay - Fully refunded'))
        ) {
            $this->addOrderState('Riverty - Awaiting payment');
            $this->addOrderState('Riverty - Authorized');
            $this->addOrderState('Riverty - Rejected');
            $this->addOrderState('Riverty - Validation issue');
            $this->addOrderState('Riverty - Captured');
            $this->addOrderState('Riverty - Partially refunded');
            $this->addOrderState('Riverty - Fully refunded');
        } else {
            include(dirname(__FILE__) . '/upgrade/Upgrade-1.5.0.php');
            upgrade_module_1_5_0();
        }

        // Prepare tab
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminRivertyAjax';
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'AdminRivertyAjax';
        }
        $tab->id_parent = 0;
        $tab->module = $this->name;

        return parent::install() &&
            $tab->add() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('displayOrderConfirmation');
    }

    /**
     * Execute actions on uninstallation of the module.
     */
    public function uninstall()
    {
        $id_tab = (int)Tab::getIdFromClassName('AdminRivertyAjax');
        if ($id_tab) {
            $tab = new Tab($id_tab);
            $tab->delete();
        }

        Configuration::deleteByName('AFTERPAY_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $output = [];

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitAfterpayModule')) == true) {
            $output[] = $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('form_settings_general', $this->renderForm());
        $this->context->smarty->assign('form_settings_ordermanagement', $this->renderForm(null, 'ordermanagement'));
        $this->context->smarty->assign('form_settings_at_digitalinvoice', $this->renderForm('at', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_at_directdebit', $this->renderForm('at', 'directdebit'));
        $this->context->smarty->assign('form_settings_at_installments', $this->renderForm('at', 'installments'));
        $this->context->smarty->assign('form_settings_be_digitalinvoice', $this->renderForm('be', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_ch_digitalinvoice', $this->renderForm('ch', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_de_digitalinvoice', $this->renderForm('de', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_de_directdebit', $this->renderForm('de', 'directdebit'));
        $this->context->smarty->assign('form_settings_de_installments', $this->renderForm('de', 'installments'));
        $this->context->smarty->assign('form_settings_nl_digitalinvoice', $this->renderForm('nl', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_nl_directdebit', $this->renderForm('nl', 'directdebit'));
        $this->context->smarty->assign('form_settings_nl_business', $this->renderForm('nl', 'business'));
        $this->context->smarty->assign('form_settings_nl_payin3', $this->renderForm('nl', 'payin3'));
        $this->context->smarty->assign('form_settings_no_digitalinvoice', $this->renderForm('no', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_se_digitalinvoice', $this->renderForm('se', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_fi_digitalinvoice', $this->renderForm('fi', 'digitalinvoice'));
        $this->context->smarty->assign('form_settings_dk_digitalinvoice', $this->renderForm('dk', 'digitalinvoice'));

        $output[] = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return implode($output);
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm($country = null, $method = null)
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitAfterpayModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm($country, $method)));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm($country = null, $method = null)
    {
        if ($country && $method) {
            $key = $country . '_' . $method;
        } elseif ($country == null && $method == 'ordermanagement') {
            $key = $method;
        } else {
            $key = 'general';
        }

        // Get available order statuses.
        $states = new OrderState();
        $orderStates = $states->getOrderStates(1);
        $stateOptions = array();
        $stateOptions[] = [
            'id_option' => null,
            'name' => ''
        ];
        foreach ($orderStates as $state) {
            $stateOptions[] = [
                'id_option' => $state['id_order_state'],
                'name' => $state['name']
            ];
        }

        switch ($key) {
            default:
                $configForm = [
                    'form' => [
                        'legend' => [
                            'title' => 'Settings',
                            'icon' => 'icon-cogs',
                        ],
                        'input' => [
                            [
                                'type' => 'switch',
                                'label' => 'Enabled',
                                'name' => 'AFTERPAY_ENABLED',
                                'is_bool' => true,
                                'desc' => 'Use the Riverty payment methods',
                                'values' => [
                                    [
                                        'id' => 'active_on',
                                        'value' => true,
                                        'label' => 'Enabled'
                                    ],
                                    [
                                        'id' => 'active_off',
                                        'value' => false,
                                        'label' => 'Disabled'
                                    ]
                                ]
                            ],
                            [
                                'type' => 'switch',
                                'label' => 'Debug log',
                                'name' => 'AFTERPAY_DEBUG_LOG',
                                'is_bool' => true,
                                'desc' =>
                                'Use debug log to write the complete Riverty request to a log file,
                                for debug functionality only.',
                                'values' => [
                                    [
                                        'id' => 'active_on',
                                        'value' => true,
                                        'label' => 'Enabled'
                                    ],
                                    [
                                        'id' => 'active_off',
                                        'value' => false,
                                        'label' => 'Disabled'
                                    ]
                                ]
                            ],
                            [
                                'type' => 'text',
                                'label' => 'Debug email',
                                'name' => 'AFTERPAY_DEBUG_EMAIL',
                                'desc' =>
                                'Use debug mail to send the complete Riverty request to your mail,
                                for debug functionality only. Leave empty to disable.'

                            ],
                            [
                                'type' => 'text',
                                'label' => 'IP Restriction',
                                'name' => 'AFTERPAY_IP_RESTRICTION',
                                'desc' =>
                                'Fill in IP address to only show the payment method for that specific IP address.
                                Leave empty to disable.'

                            ]
                        ],
                        'submit' => [
                            'title' => 'Save',
                        ]
                    ]
                ];
                break;
            case 'ordermanagement':
                $configForm = [
                    'form' => [
                        'legend' => [
                            'title' => 'Order management',
                            'icon' => 'icon-cogs',
                        ],
                        'input' => [
                            [
                                'type' => 'select',
                                'label' => 'Use captures',
                                'name' => 'AFTERPAY_USE_CAPTURES',
                                'is_bool' => true,
                                'desc' => 'Captures orders that are authorized',
                                'options' => [
                                    'query' => [
                                        [
                                            'id_option' => 'no',
                                            'name' => 'No'
                                        ],
                                        [
                                            'id_option' => 'yes-autocapture',
                                            'name' => 'Yes, automatically after authorization'
                                        ],
                                        [
                                            'id_option' => 'yes-onstatus',
                                            'name' => 'Yes, based on order status'
                                        ]
                                    ],
                                    'id' => 'id_option',
                                    'name' => 'name'
                                ],
                                'values' => [
                                    [
                                        'id' => 'active_on',
                                        'value' => true,
                                        'label' => 'Enabled'
                                    ],
                                    [
                                        'id' => 'active_off',
                                        'value' => false,
                                        'label' => 'Disabled'
                                    ]
                                ]
                            ],
                            [
                                'type' => 'select',
                                'label' => 'Status to base capture on',
                                'name' => 'AFTERPAY_CAPTURE_STATUS',
                                'desc' =>
                                'When this status is assiged to the order, a capture request towards Riverty will be
executed',
                                'options' => [
                                    'query' => $stateOptions,
                                    'id' => 'id_option',
                                    'name' => 'name'
                                ]
                            ],
                            [
                                'type' => 'switch',
                                'label' => 'Use refunds',
                                'name' => 'AFTERPAY_USE_REFUNDS',
                                'is_bool' => true,
                                'desc' => 'Refund Riverty orders that are credited',
                                'values' => [
                                    [
                                        'id' => 'active_on',
                                        'value' => true,
                                        'label' => 'Enabled'
                                    ],
                                    [
                                        'id' => 'active_off',
                                        'value' => false,
                                        'label' => 'Disabled'
                                    ]
                                ]
                            ]
                        ],
                        'submit' => [
                            'title' => 'Save',
                        ]
                    ]
                ];
                break;
            case 'at_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('at', $paymentMethod);
                break;
            case 'at_directdebit':
                $paymentMethod = [
                    'title' => 'Direct Debit',
                    'code' => 'DIRECTDEBIT'
                ];
                $configForm = $this->getPaymentSettingsForm('at', $paymentMethod);
                break;
            case 'at_installments':
                $paymentMethod = [
                    'title' => 'Installments',
                    'code' => 'INSTALLMENTS'
                ];
                $configForm = $this->getPaymentSettingsForm('at', $paymentMethod);
                break;
            case 'be_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('be', $paymentMethod);
                break;
            case 'ch_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('ch', $paymentMethod);
                break;
            case 'de_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('de', $paymentMethod);
                break;
            case 'de_directdebit':
                $paymentMethod = [
                    'title' => 'Direct Debit',
                    'code' => 'DIRECTDEBIT'
                ];
                $configForm = $this->getPaymentSettingsForm('de', $paymentMethod);
                break;
            case 'de_installments':
                $paymentMethod = [
                    'title' => 'Installments',
                    'code' => 'INSTALLMENTS'
                ];
                $configForm = $this->getPaymentSettingsForm('de', $paymentMethod);
                break;
            case 'nl_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('nl', $paymentMethod);
                break;
            case 'nl_directdebit':
                $paymentMethod = [
                    'title' => 'Direct Debit',
                    'code' => 'DIRECTDEBIT'
                ];
                $configForm = $this->getPaymentSettingsForm('nl', $paymentMethod);
                break;
            case 'nl_payin3':
                $paymentMethod = [
                    'title' => 'Pay in 3',
                    'code' => 'PAYIN3'
                ];
                $configForm = $this->getPaymentSettingsForm('nl', $paymentMethod);
                break;
            case 'nl_business':
                $paymentMethod = [
                    'title' => 'Business 2 Business',
                    'code' => 'BUSINESS'
                ];
                $configForm = $this->getPaymentSettingsForm('nl', $paymentMethod);
                break;
            case 'no_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('no', $paymentMethod);
                break;
            case 'se_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('se', $paymentMethod);
                break;
            case 'fi_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('fi', $paymentMethod);
                break;
            case 'dk_digitalinvoice':
                $paymentMethod = [
                    'title' => '14-day Invoice',
                    'code' => 'DIGITALINVOICE'
                ];
                $configForm = $this->getPaymentSettingsForm('dk', $paymentMethod);
                break;
        }
        return $configForm;
    }

    /**
     * Create the basic settings for an AfterPay payment method.
     * @param string $country
     * @param array  $paymentMethod
     * @return array
     */
    protected function getPaymentSettingsForm($country, $paymentMethod)
    {
        $baseForm = [
            'form' => [
                'legend' => [
                    'title' => $paymentMethod['title'],
                    'icon' => 'icon-cogs',
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => 'Enabled',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_ENABLED',
                        'is_bool' => true,
                        'desc' => 'Enable payment method',
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => 'Enabled'
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => 'Disabled'
                            ]
                        ]
                    ],
                    [
                        'type' => 'hidden',
                        'label' => 'Title',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_TITLE',
                        'desc' => 'This controls the title which the user sees during checkout.'
                    ],
                    [
                        'type' => 'hidden',
                        'label' => 'Description',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_DESCRIPTION',
                        'desc' => 'This controls the description which the user sees during checkout.'
                    ],
                    [
                        'type' => 'hidden',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_TERMS',
                    ],
                    [
                        'type' => 'text',
                        'label' => 'Test mode API key',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_TEST_API_KEY',
                        'desc' =>
                        'Connect to the Riverty test environment and place test orders. New to Riverty? 
                            <a href="https://www.riverty.com/en/business/products/get-started-riverty-buy-now-pay-later/" target ="_blank">
                            Sign up here</a> to get access and a test API key on the <a href="https://merchantportal-pt.riverty.com/login" target="_blank">
                            Riverty Merchant Portal.</a>'
                    ],
                    [
                        'type' => 'text',
                        'label' => 'Sandbox mode API key',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SANDBOX_API_KEY',
                        'desc' =>
                        'Explore the Riverty products easily via the Sandbox connection. Get your API key on the 
                            <a href="https://developer.riverty.com/" target="_blank">Riverty Developer Portal.</a>'
                    ],
                    [
                        'type' => 'text',
                        'label' => 'Production mode API key',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_PRODUCTION_API_KEY',
                        'desc' =>
                        'Start accepting payment by connecting to the Riverty production environment. Get your production API key on the
                            <a href="https://merchant.afterpay.io/" target="_blank">Riverty Merchant Portal.</a>'
                    ],
                    [
                        'type' => 'text',
                        'label' => 'Lower threshold',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_LOWER_THRESHOLD',
                        'desc' =>
                        'Disable Riverty payment method if Cart Total is lower than the specified value.
                            Leave blank to disable this feature.'

                    ],
                    [
                        'type' => 'text',
                        'label' => 'Upper threshold',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_UPPER_THRESHOLD',
                        'desc' =>
                        'Disable Riverty payment method if Cart Total is higher than the specified value.
                            Leave blank to disable this feature.'

                    ],
                    [
                        'type' => 'select',
                        'label' => 'Connection mode',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_TESTMODE',
                        'desc' =>
                        'To test the payment method, select ‘Sandbox’ or ‘Test’. When going live, select ‘Production’.',
                        'options' => [
                            'query' => [
                                [
                                    'id_option' => 'production',
                                    'name' => 'Production'
                                ],
                                [
                                    'id_option' => 'test',
                                    'name' => 'Test'
                                ],
                                [
                                    'id_option' => 'sandbox',
                                    'name' => 'Sandbox'
                                ]
                            ],
                            'id' => 'id_option',
                            'name' => 'name'
                        ]
                    ],
                    [
                        'type' => 'switch',
                        'label' => 'Show phonenumber',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_PHONE',
                        'is_bool' => true,
                        'desc' => 'Show phonenumber field in Riverty form in the checkout',
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => 'Show'
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => 'Hide'
                            ]
                        ]
                    ],
                    [
                        'type' => 'switch',
                        'label' => 'Show birthdate',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_DOB',
                        'is_bool' => true,
                        'desc' => 'Show birthdate field in Riverty form in the checkout',
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => 'Show'
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => 'Hide'
                            ]
                        ]
                    ],

                    [
                        'type' => 'switch',
                        'label' => 'Show SSN number',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_SSN',
                        'is_bool' => true,
                        'desc' => 'Show SSN number field in Riverty form in the checkout',
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => 'Show'
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => 'Hide'
                            ]
                        ]
                    ],
                    [
                        'type' => 'hidden',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SSN_PLACEHOLDER',
                    ],
                    [
                        'type' => 'hidden',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SSN_MAXLENGTH',
                    ],
                ],
                'submit' => [
                    'title' => 'Save',
                ],
                'buttons' => array(
                    'test-api' => array(
                        'title' => "Test API key",
                        'type' => 'button',
                        'class' => 'btn btn-default pull-right AFTERPAY_TEST_API_BUTTON',
                        'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_TEST_API',
                    ),
                ),
            ]
        ];

        if ($country == 'at' || $country == 'ch' || $country == 'de') {
            array_push(
                $baseForm['form']['input'],
                [
                    'type' => 'select',
                    'label' => 'Profile tracking setup',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_PROFILE_TRACKING',
                    'desc' => 'Enable or disable profile tracking.',
                    'options' => [
                        'query' => [
                            [
                                'id_option' => 'Enabled-mandatory',
                                'name' => 'Enabled - mandatory'
                            ],
                            [
                                'id_option' => 'Enabled-optional',
                                'name' => 'Enabled - optional'
                            ],
                            [
                                'id_option' => 'Disabled',
                                'name' => 'Disabled'
                            ]
                        ],
                        'id' => 'id_option',
                        'name' => 'name'
                    ]
                ],
                [
                    'type' => 'text',
                    'label' => 'Profile Tracking ID',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_PROFILE_TRACKING_ID',
                ]
            );
        }
        switch ($paymentMethod['code']) {
            case 'DIGITALINVOICE':
                return $baseForm;
            case 'PAYIN3':
                return $baseForm;
            case 'DIRECTDEBIT':
                $baseForm['form']['input'][] = [
                    'type' => 'switch',
                    'label' => 'Show Bank IBAN input field',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_IBAN',
                    'is_bool' => true,
                    'desc' => 'Show input field for IBAN in Riverty form in the checkout',
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => true,
                            'label' => 'Show'
                        ],
                        [
                            'id' => 'active_off',
                            'value' => false,
                            'label' => 'Hide'
                        ]
                    ]
                ];
                return $baseForm;
            case 'INSTALLMENTS':
                $baseForm['form']['input'][] = [
                    'type' => 'switch',
                    'label' => 'Show Bank IBAN input field',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_IBAN',
                    'is_bool' => true,
                    'desc' => 'Show input field for IBAN in Riverty form in the checkout',
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => true,
                            'label' => 'Show'
                        ],
                        [
                            'id' => 'active_off',
                            'value' => false,
                            'label' => 'Hide'
                        ]
                    ]
                ];
                return $baseForm;
            case 'BUSINESS':
                $baseForm['form']['input'][] = [
                    'type' => 'switch',
                    'label' => 'Show company name input field',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_COMPANY',
                    'is_bool' => true,
                    'desc' => 'Show input field for company name in Riverty form in the checkout',
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => true,
                            'label' => 'Show'
                        ],
                        [
                            'id' => 'active_off',
                            'value' => false,
                            'label' => 'Hide'
                        ]
                    ]
                ];
                $baseForm['form']['input'][] = [
                    'type' => 'switch',
                    'label' => 'Show chamber of commerce number input field',
                    'name' => 'AFTERPAY_' . Tools::strtoupper($country) . '_' . $paymentMethod['code'] . '_SHOW_COC',
                    'is_bool' => true,
                    'desc' =>
                    'Show input field for chamber of commerce number in Riverty form in the checkout',
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => true,
                            'label' => 'Show'
                        ],
                        [
                            'id' => 'active_off',
                            'value' => false,
                            'label' => 'Hide'
                        ]
                    ]
                ];
                return $baseForm;
        }
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $settings = [];
        $defaultSettings = [
            'AFTERPAY_ENABLED' => false,
            'AFTERPAY_DEBUG_LOG' => false,
            'AFTERPAY_DEBUG_EMAIL' => '',
            'AFTERPAY_IP_RESTRICTION' => '',
            'AFTERPAY_USE_CAPTURES' => 'no',
            'AFTERPAY_CAPTURE_STATUS' => null,
            'AFTERPAY_USE_REFUNDS' => false,
            // AT Digital Invoice
            'AFTERPAY_AT_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_AT_DIGITALINVOICE_TITLE' => 'Rechnung',
            'AFTERPAY_AT_DIGITALINVOICE_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_AT_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_at/',
            'AFTERPAY_AT_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_AT_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_AT_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_AT_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_AT_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_AT_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_AT_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_AT_DIGITALINVOICE_SHOW_DOB' => true,
            'AFTERPAY_AT_DIGITALINVOICE_SHOW_SSN' => false,
            'AFTERPAY_AT_DIGITALINVOICE_SSN_PLACEHOLDER' => '',
            'AFTERPAY_AT_DIGITALINVOICE_SSN_MAXLENGTH' => 25,
            'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING_ID' => '',
            // AT Direct Debit
            'AFTERPAY_AT_DIRECTDEBIT_ENABLED' => false,
            'AFTERPAY_AT_DIRECTDEBIT_TITLE' => 'Lastschrift',
            'AFTERPAY_AT_DIRECTDEBIT_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_AT_DIRECTDEBIT_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_at/',
            'AFTERPAY_AT_DIRECTDEBIT_TEST_API_KEY' => '',
            'AFTERPAY_AT_DIRECTDEBIT_PRODUCTION_API_KEY' => '',
            'AFTERPAY_AT_DIRECTDEBIT_SANDBOX_API_KEY' => '',
            'AFTERPAY_AT_DIRECTDEBIT_LOWER_THRESHOLD' => 5,
            'AFTERPAY_AT_DIRECTDEBIT_UPPER_THRESHOLD' => '',
            'AFTERPAY_AT_DIRECTDEBIT_TESTMODE' => 'yes',
            'AFTERPAY_AT_DIRECTDEBIT_SHOW_PHONE' => false,
            'AFTERPAY_AT_DIRECTDEBIT_SHOW_DOB' => true,
            'AFTERPAY_AT_DIRECTDEBIT_SHOW_IBAN' => true,
            'AFTERPAY_AT_DIRECTDEBIT_SHOW_SSN' => false,
            'AFTERPAY_AT_DIRECTDEBIT_SSN_PLACEHOLDER' => '',
            'AFTERPAY_AT_DIRECTDEBIT_SSN_MAXLENGTH' => 25,
            'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING_ID' => '',
            // AT Installments
            'AFTERPAY_AT_INSTALLMENTS_ENABLED' => false,
            'AFTERPAY_AT_INSTALLMENTS_TITLE' => 'Rechnung',
            'AFTERPAY_AT_INSTALLMENTS_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_AT_INSTALLMENTS_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_at/',
            'AFTERPAY_AT_INSTALLMENTS_TEST_API_KEY' => '',
            'AFTERPAY_AT_INSTALLMENTS_PRODUCTION_API_KEY' => '',
            'AFTERPAY_AT_INSTALLMENTS_SANDBOX_API_KEY' => '',
            'AFTERPAY_AT_INSTALLMENTS_LOWER_THRESHOLD' => 5,
            'AFTERPAY_AT_INSTALLMENTS_UPPER_THRESHOLD' => '',
            'AFTERPAY_AT_INSTALLMENTS_TESTMODE' => 'yes',
            'AFTERPAY_AT_INSTALLMENTS_SHOW_PHONE' => false,
            'AFTERPAY_AT_INSTALLMENTS_SHOW_DOB' => true,
            'AFTERPAY_AT_INSTALLMENTS_SHOW_IBAN' => true,
            'AFTERPAY_AT_INSTALLMENTS_SHOW_SSN' => false,
            'AFTERPAY_AT_INSTALLMENTS_SSN_PLACEHOLDER' => '',
            'AFTERPAY_AT_INSTALLMENTS_SSN_MAXLENGTH' => 25,
            'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING_ID' => '',
            // BE Digital Invoice
            'AFTERPAY_BE_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_BE_DIGITALINVOICE_TITLE' => 'Veilig achteraf betalen',
            'AFTERPAY_BE_DIGITALINVOICE_DESCRIPTION' => '',
            'AFTERPAY_BE_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/nl_be/',
            'AFTERPAY_BE_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_BE_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_BE_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_BE_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_BE_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_BE_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_BE_DIGITALINVOICE_SHOW_PHONE' => true,
            'AFTERPAY_BE_DIGITALINVOICE_SHOW_DOB' => true,
            'AFTERPAY_BE_DIGITALINVOICE_SHOW_SSN' => false,
            'AFTERPAY_BE_DIGITALINVOICE_SSN_PLACEHOLDER' => '',
            'AFTERPAY_BE_DIGITALINVOICE_SSN_MAXLENGTH' => 25,
            // CH Digital Invoice
            'AFTERPAY_CH_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_CH_DIGITALINVOICE_TITLE' => 'Rechnung',
            'AFTERPAY_CH_DIGITALINVOICE_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_CH_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_ch/',
            'AFTERPAY_CH_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_CH_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_CH_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_CH_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_CH_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_CH_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_CH_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_CH_DIGITALINVOICE_SHOW_DOB' => true,
            'AFTERPAY_CH_DIGITALINVOICE_SHOW_SSN' => false,
            'AFTERPAY_CH_DIGITALINVOICE_SSN_PLACEHOLDER' => '',
            'AFTERPAY_CH_DIGITALINVOICE_SSN_MAXLENGTH' => 25,
            'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING_ID' => '',
            // DE Digital Invoice
            'AFTERPAY_DE_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_DE_DIGITALINVOICE_TITLE' => 'Rechnung',
            'AFTERPAY_DE_DIGITALINVOICE_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_DE_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_de/',
            'AFTERPAY_DE_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_DE_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_DE_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_DE_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_DE_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_DE_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_DE_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_DE_DIGITALINVOICE_SHOW_DOB' => true,
            'AFTERPAY_DE_DIGITALINVOICE_SHOW_SSN' => false,
            'AFTERPAY_DE_DIGITALINVOICE_SSN_PLACEHOLDER' => '',
            'AFTERPAY_DE_DIGITALINVOICE_SSN_MAXLENGTH' => 25,
            'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING_ID' => '',
            // DE Direct Debit
            'AFTERPAY_DE_DIRECTDEBIT_ENABLED' => false,
            'AFTERPAY_DE_DIRECTDEBIT_TITLE' => 'Lastschrift',
            'AFTERPAY_DE_DIRECTDEBIT_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_DE_DIRECTDEBIT_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_de/',
            'AFTERPAY_DE_DIRECTDEBIT_TEST_API_KEY' => '',
            'AFTERPAY_DE_DIRECTDEBIT_PRODUCTION_API_KEY' => '',
            'AFTERPAY_DE_DIRECTDEBIT_SANDBOX_API_KEY' => '',
            'AFTERPAY_DE_DIRECTDEBIT_LOWER_THRESHOLD' => 5,
            'AFTERPAY_DE_DIRECTDEBIT_UPPER_THRESHOLD' => '',
            'AFTERPAY_DE_DIRECTDEBIT_TESTMODE' => 'yes',
            'AFTERPAY_DE_DIRECTDEBIT_SHOW_PHONE' => false,
            'AFTERPAY_DE_DIRECTDEBIT_SHOW_DOB' => true,
            'AFTERPAY_DE_DIRECTDEBIT_SHOW_IBAN' => true,
            'AFTERPAY_DE_DIRECTDEBIT_SHOW_SSN' => false,
            'AFTERPAY_DE_DIRECTDEBIT_SSN_PLACEHOLDER' => '',
            'AFTERPAY_DE_DIRECTDEBIT_SSN_MAXLENGTH' => 25,
            'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING_ID' => '',
            // DE Installments
            'AFTERPAY_DE_INSTALLMENTS_ENABLED' => false,
            'AFTERPAY_DE_INSTALLMENTS_TITLE' => 'Rechnung',
            'AFTERPAY_DE_INSTALLMENTS_DESCRIPTION' => 'Erst probieren, dann zahlen',
            'AFTERPAY_DE_INSTALLMENTS_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/de_at/',
            'AFTERPAY_DE_INSTALLMENTS_TEST_API_KEY' => '',
            'AFTERPAY_DE_INSTALLMENTS_PRODUCTION_API_KEY' => '',
            'AFTERPAY_DE_INSTALLMENTS_SANDBOX_API_KEY' => '',
            'AFTERPAY_DE_INSTALLMENTS_LOWER_THRESHOLD' => 5,
            'AFTERPAY_DE_INSTALLMENTS_UPPER_THRESHOLD' => '',
            'AFTERPAY_DE_INSTALLMENTS_TESTMODE' => 'yes',
            'AFTERPAY_DE_INSTALLMENTS_SHOW_PHONE' => false,
            'AFTERPAY_DE_INSTALLMENTS_SHOW_DOB' => true,
            'AFTERPAY_DE_INSTALLMENTS_SHOW_IBAN' => true,
            'AFTERPAY_DE_INSTALLMENTS_SHOW_SSN' => false,
            'AFTERPAY_DE_INSTALLMENTS_SSN_PLACEHOLDER' => '',
            'AFTERPAY_DE_INSTALLMENTS_SSN_MAXLENGTH' => 25,
            'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING' => 'Disabled',
            'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING_ID' => '',
            // NL Digital Invoice
            'AFTERPAY_NL_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_NL_DIGITALINVOICE_TITLE' => 'Veilig achteraf betalen',
            'AFTERPAY_NL_DIGITALINVOICE_DESCRIPTION' => '',
            'AFTERPAY_NL_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/nl_nl/',
            'AFTERPAY_NL_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_NL_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_NL_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_NL_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_NL_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_NL_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_NL_DIGITALINVOICE_SHOW_PHONE' => true,
            'AFTERPAY_NL_DIGITALINVOICE_SHOW_DOB' => true,
            'AFTERPAY_NL_DIGITALINVOICE_SHOW_SSN' => false,
            'AFTERPAY_NL_DIGITALINVOICE_SSN_PLACEHOLDER' => '',
            'AFTERPAY_NL_DIGITALINVOICE_SSN_MAXLENGTH' => 25,
            // NL Pay in 3
            'AFTERPAY_NL_PAYIN3_ENABLED' => false,
            'AFTERPAY_NL_PAYIN3_TITLE' => 'Zahlen Sie in 3 Raten, keine Zinsen, keine Gebühr',
            'AFTERPAY_NL_PAYIN3_DESCRIPTION' => '',
            'AFTERPAY_NL_PAYIN3_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/nl_nl/',
            'AFTERPAY_NL_PAYIN3_TEST_API_KEY' => '',
            'AFTERPAY_NL_PAYIN3_PRODUCTION_API_KEY' => '',
            'AFTERPAY_NL_PAYIN3_SANDBOX_API_KEY' => '',
            'AFTERPAY_NL_PAYIN3_LOWER_THRESHOLD' => 5,
            'AFTERPAY_NL_PAYIN3_UPPER_THRESHOLD' => '',
            'AFTERPAY_NL_PAYIN3_TESTMODE' => 'yes',
            'AFTERPAY_NL_PAYIN3_SHOW_PHONE' => true,
            'AFTERPAY_NL_PAYIN3_SHOW_DOB' => true,
            'AFTERPAY_NL_PAYIN3_SHOW_SSN' => false,
            'AFTERPAY_NL_PAYIN3_SSN_PLACEHOLDER' => '',
            'AFTERPAY_NL_PAYIN3_SSN_MAXLENGTH' => 25,
            // NL Direct Debit
            'AFTERPAY_NL_DIRECTDEBIT_ENABLED' => false,
            'AFTERPAY_NL_DIRECTDEBIT_TITLE' => 'Veilig achteraf betalen met eenmalige machtiging',
            'AFTERPAY_NL_DIRECTDEBIT_DESCRIPTION' => '',
            'AFTERPAY_NL_DIRECTDEBIT_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/nl_nl/',
            'AFTERPAY_NL_DIRECTDEBIT_TEST_API_KEY' => '',
            'AFTERPAY_NL_DIRECTDEBIT_PRODUCTION_API_KEY' => '',
            'AFTERPAY_NL_DIRECTDEBIT_SANDBOX_API_KEY' => '',
            'AFTERPAY_NL_DIRECTDEBIT_LOWER_THRESHOLD' => 5,
            'AFTERPAY_NL_DIRECTDEBIT_UPPER_THRESHOLD' => '',
            'AFTERPAY_NL_DIRECTDEBIT_TESTMODE' => 'yes',
            'AFTERPAY_NL_DIRECTDEBIT_SHOW_PHONE' => true,
            'AFTERPAY_NL_DIRECTDEBIT_SHOW_DOB' => true,
            'AFTERPAY_NL_DIRECTDEBIT_SHOW_IBAN' => true,
            'AFTERPAY_NL_DIRECTDEBIT_SHOW_SSN' => false,
            'AFTERPAY_NL_DIRECTDEBIT_SSN_PLACEHOLDER' => '',
            'AFTERPAY_NL_DIRECTDEBIT_SSN_MAXLENGTH' => 25,
            // NL Business 2 Business
            'AFTERPAY_NL_BUSINESS_ENABLED' => false,
            'AFTERPAY_NL_BUSINESS_TITLE' => 'Veilig achteraf betalen',
            'AFTERPAY_NL_BUSINESS_DESCRIPTION' => '',
            'AFTERPAY_NL_BUSINESS_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/nl_nl/',
            'AFTERPAY_NL_BUSINESS_TEST_API_KEY' => '',
            'AFTERPAY_NL_BUSINESS_PRODUCTION_API_KEY' => '',
            'AFTERPAY_NL_BUSINESS_SANDBOX_API_KEY' => '',
            'AFTERPAY_NL_BUSINESS_LOWER_THRESHOLD' => 5,
            'AFTERPAY_NL_BUSINESS_UPPER_THRESHOLD' => '',
            'AFTERPAY_NL_BUSINESS_TESTMODE' => 'yes',
            'AFTERPAY_NL_BUSINESS_SHOW_PHONE' => true,
            'AFTERPAY_NL_BUSINESS_SHOW_DOB' => false,
            'AFTERPAY_NL_BUSINESS_SHOW_COMPANY' => true,
            'AFTERPAY_NL_BUSINESS_SHOW_COC' => true,
            'AFTERPAY_NL_BUSINESS_SHOW_SSN' => false,
            'AFTERPAY_NL_BUSINESS_SSN_PLACEHOLDER' => '',
            'AFTERPAY_NL_BUSINESS_SSN_MAXLENGTH' => 25,
            // NO Digital Invoice
            'AFTERPAY_NO_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_NO_DIGITALINVOICE_TITLE' => 'Faktura 14 dagar',
            'AFTERPAY_NO_DIGITALINVOICE_DESCRIPTION' => 'Handle først, betal senere',
            'AFTERPAY_NO_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/no_no/',
            'AFTERPAY_NO_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_NO_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_NO_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_NO_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_NO_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_NO_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_NO_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_NO_DIGITALINVOICE_SHOW_DOB' => false,
            'AFTERPAY_NO_DIGITALINVOICE_SHOW_SSN' => true,
            'AFTERPAY_NO_DIGITALINVOICE_SSN_PLACEHOLDER' => 'DDMMÅÅXXXXX',
            'AFTERPAY_NO_DIGITALINVOICE_SSN_MAXLENGTH' => 11,
            // SE Digital Invoice
            'AFTERPAY_SE_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_SE_DIGITALINVOICE_TITLE' => 'Faktura 14 dagar',
            'AFTERPAY_SE_DIGITALINVOICE_DESCRIPTION' => 'Upplev först, betala senare',
            'AFTERPAY_SE_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/sv_se/',
            'AFTERPAY_SE_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_SE_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_SE_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_SE_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_SE_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_SE_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_SE_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_SE_DIGITALINVOICE_SHOW_DOB' => false,
            'AFTERPAY_SE_DIGITALINVOICE_SHOW_SSN' => true,
            'AFTERPAY_SE_DIGITALINVOICE_SSN_PLACEHOLDER' => 'ÅÅÅÅMMDDNNNN',
            'AFTERPAY_SE_DIGITALINVOICE_SSN_MAXLENGTH' => 12,
            // FI Digital Invoice
            'AFTERPAY_FI_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_FI_DIGITALINVOICE_TITLE' => 'Maksa myöhemmin',
            'AFTERPAY_FI_DIGITALINVOICE_DESCRIPTION' => 'Kokeile ensin, maksa 14 päivän kuluessa',
            'AFTERPAY_FI_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/fi_fi/',
            'AFTERPAY_FI_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_FI_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_FI_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_FI_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_FI_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_FI_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_FI_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_FI_DIGITALINVOICE_SHOW_DOB' => false,
            'AFTERPAY_FI_DIGITALINVOICE_SHOW_SSN' => true,
            'AFTERPAY_FI_DIGITALINVOICE_SSN_PLACEHOLDER' => 'PPKKVV-XXXX',
            'AFTERPAY_FI_DIGITALINVOICE_SSN_MAXLENGTH' => 11,
            // DK Digital Invoice
            'AFTERPAY_DK_DIGITALINVOICE_ENABLED' => false,
            'AFTERPAY_DK_DIGITALINVOICE_TITLE' => 'Faktura 14 dage',
            'AFTERPAY_DK_DIGITALINVOICE_DESCRIPTION' => 'Oplev først, betal senere',
            'AFTERPAY_DK_DIGITALINVOICE_TERMS' => 'https://documents.myafterpay.com/consumer-terms-conditions/da_dk/',
            'AFTERPAY_DK_DIGITALINVOICE_TEST_API_KEY' => '',
            'AFTERPAY_DK_DIGITALINVOICE_PRODUCTION_API_KEY' => '',
            'AFTERPAY_DK_DIGITALINVOICE_SANDBOX_API_KEY' => '',
            'AFTERPAY_DK_DIGITALINVOICE_LOWER_THRESHOLD' => 5,
            'AFTERPAY_DK_DIGITALINVOICE_UPPER_THRESHOLD' => '',
            'AFTERPAY_DK_DIGITALINVOICE_TESTMODE' => 'yes',
            'AFTERPAY_DK_DIGITALINVOICE_SHOW_PHONE' => false,
            'AFTERPAY_DK_DIGITALINVOICE_SHOW_DOB' => false,
            'AFTERPAY_DK_DIGITALINVOICE_SHOW_SSN' => true,
            'AFTERPAY_DK_DIGITALINVOICE_SSN_PLACEHOLDER' => 'DDMMÅÅXXXX',
            'AFTERPAY_DK_DIGITALINVOICE_SSN_MAXLENGTH' => 10,
        ];

        foreach ($defaultSettings as $defaultSettingsKey => $defaultSettingsValue) {
            $settings[$defaultSettingsKey] = Configuration::hasKey($defaultSettingsKey) ?
                Configuration::get($defaultSettingsKey) : $defaultSettingsValue;
        }

        return $settings;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $formValues = $this->getConfigFormValues();

        foreach (array_keys($formValues) as $key) {
            if (Tools::getIsset($key)) {
                Configuration::updateValue($key, Tools::getValue($key));


                // Set Confirmation message settings

                if (stripos($key, 'USE_REFUNDS')) {
                    $settingsName = 'Order management';
                } else if (stripos($key, 'DIGITALINVOICE')) {
                    $settingsName = '14-Day Invoice';
                } else if (stripos($key, 'DIRECTDEBIT')) {
                    $settingsName = 'Direct Debit';
                } else if (stripos($key, 'BUSINESS')) {
                    $settingsName = 'Business 2 Business';
                } else if (stripos($key, 'PAYIN3')) {
                    $settingsName = 'Pay in 3';
                } else {
                    $settingsName = 'Settings';
                }
            }
        }
        return $this->displayConfirmation($this->l($settingsName . ' saved successfully'));
    }

    /**
     * Write debug messages to afterpay.log file in /log/afterpay.log
     * @param string $logMessage
     * @return void
     */
    public function writeLog($logMessage)
    {
        if (Configuration::hasKey('AFTERPAY_DEBUG_LOG') && Configuration::get('AFTERPAY_DEBUG_LOG') == '1') {
            $logger = new FileLogger(0);
            $logger->setFilename(_PS_ROOT_DIR_ . "/var/logs/afterpay.log");
            $logger->logDebug($logMessage);
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        $link = new Link;
        $validate_link = $link->getAdminLink('AdminRivertyAjax');
        $serverCronUrl = $this->getServerCronUrl();
        $webCronUrl = $this->getWebServerCronUrl();
        Media::addJsDef(array(
            "validate_link" => $validate_link,
            "server_cron_url" => $serverCronUrl,
            "web_cron_url" => $webCronUrl
        ));
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
        $this->context->controller->addCSS($this->_path . '/views/css/back.css');
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        // Create a link with the good path
        $link = new Link;
        $parameters = array("action" => "action_name");
        $ajax_link = $link->getModuleLink('riverty', 'ajax', $parameters);

        Media::addJsDef(array(
            "ajax_link" => $ajax_link
        ));
    }

    /**
     * Return payment options available for PS 1.7+
     *
     * @param array Hook parameters
     *
     * @return array|null
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        $cart = null;
        if (isset($params['cart'])) {
            $cart = $params['cart'];
        }
        $paymentOptions = $this->getPaymentMethods($cart);

        return $paymentOptions;
    }

    /**
     * Add extra information to thank you page
     */
    public function hookDisplayOrderConfirmation($params)

    {
        if ($params['order']->payment === 'Riverty') {
            return $this->fetch('module:riverty/views/templates/front/orderconfirmation.tpl');
        }
    }

    public function checkCurrency($cart)
    {
        $currencyOrder = new Currency($cart->id_currency);
        $currenciesModule = $this->getCurrency($cart->id_currency);
        if (is_array($currenciesModule)) {
            foreach ($currenciesModule as $currencyModule) {
                if ($currencyOrder->id == $currencyModule['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Function to get the available AfterPay payment methods.
     *
     * @param $cart object
     * @return array $paymentmethods
     */
    private function getPaymentMethods($cart = null)
    {
        /**
         * @var $cart Cart
         */
        $availablePaymentMethods = $this->getPaymentMethodsForCart($cart);

        $paymentmethods = [];
        foreach ($availablePaymentMethods as $paymentMethod) {
            $objPaymentMethod = new PaymentOption();

            $objPaymentMethod->setCallToActionText($paymentMethod['name'])
                ->setModuleName('afterpay_' . $paymentMethod['id'])
                ->setInputs([
                    'payment_option_id' => [
                        'name' => 'payment_option_id',
                        'type' => 'hidden',
                        'value' => $paymentMethod['id'],
                    ],
                ])
                ->setLogo($paymentMethod['logo']);
            if (isset($paymentMethod['description'])) {
                $objPaymentMethod->setAdditionalInformation('<p class="introText" >' . $paymentMethod['description'] . '</p>');
            }
            switch ($paymentMethod['type']) {
                case 'digitalinvoice':
                    $objPaymentMethod->setForm($this->getDigitalInvoiceForm($paymentMethod));
                    break;
                case 'payin3':
                    $objPaymentMethod->setForm($this->getPayIn3Form($paymentMethod));
                    break;
                case 'directdebit':
                    $objPaymentMethod->setForm($this->getDirectDebitForm($paymentMethod));
                    break;
                case 'installments':
                    $objPaymentMethod->setForm($this->getInstallmentsForm($paymentMethod));
                    break;
                case 'business':
                    $objPaymentMethod->setForm($this->getDigitalInvoiceForm($paymentMethod));
                    break;
            }

            $paymentmethods[] = $objPaymentMethod;
        }

        return $paymentmethods;
    }

    /**
     * Function to get the available AfterPay payment methods based on the cart
     *
     * @param $cart object
     * @return array $paymentmethods
     */
    private function getPaymentMethodsForCart($cart = null)
    {
        /**
         * @var $cart Cart
         */
        // Return listed payment methods if already checked
        if (isset($this->paymentMethods) && count($this->paymentMethods) > 0) {
            return $this->paymentMethods;
        }

        // Check if AfterPay is enabled.
        if (!Configuration::hasKey('AFTERPAY_ENABLED')) {
            return [];
        }
        if (Configuration::get('AFTERPAY_ENABLED') != "1") {
            return [];
        }

        // Get cart currency
        $currency = new Currency($cart->id_currency);
        $currencyCode = $currency->iso_code;

        // Get payment methods from configuration.
        $paymentMethods = [];

        $countries = Country::getCountries($this->context->language->id, true);
        $addressInvoice = new Address($cart->id_address_invoice);
        $countryCode = $countries[$addressInvoice->id_country]['iso_code'];

        foreach ($this->afterpayPaymentMethods as $country => $afterpayPaymentMethods) {
            // Only show payment methods which support the used currency.
            if (!in_array($country, $this->afterpayCurrencyRestrictions[$currencyCode])) {
                continue;
            }
            // Only show payment methods for the selected country.
            if ($country != strtolower($countryCode)) {
                continue;
            }
            foreach ($afterpayPaymentMethods as $afterpayPaymentMethod) {
                $paymentMethodId = Tools::strtoupper($country) . '_' . Tools::strtoupper($afterpayPaymentMethod);
                $key = 'AFTERPAY_' . $paymentMethodId;

                // Check if payment method is enabled.
                if (Configuration::hasKey($key . '_ENABLED') && Configuration::get($key . '_ENABLED') == "1") {
                    // Check if cart amount is more than lower threshold.
                    if (Configuration::hasKey($key . '_LOWER_THRESHOLD')) {
                        $lowerTreshold = Configuration::get($key . '_LOWER_THRESHOLD');
                        if ($lowerTreshold != "" && $lowerTreshold > $cart->getordertotal()) {
                            continue;
                        }
                    }

                    // Check if cart amount is lower than the upper threshold.
                    if (Configuration::hasKey($key . '_UPPER_THRESHOLD')) {
                        $upperTreshold = Configuration::get($key . '_UPPER_THRESHOLD');
                        if ($upperTreshold != "" && $upperTreshold <= $cart->getordertotal()) {
                            continue;
                        }
                    }
                    $name = Configuration::get($key . '_TITLE');
                    $description = Configuration::get($key . '_DESCRIPTION');

                    // Check if it's available at AfterPay and get the name and description.

                    $availablePaymentMethodInfo = $this->getAvailablePaymentMethod(
                        $cart->getordertotal(),
                        $paymentMethodId,
                        $afterpayPaymentMethod,
                        $cart
                    );
                    if ($availablePaymentMethodInfo['title'] == '') {
                        continue;
                    } else {
                        $name = $availablePaymentMethodInfo['title'];
                        $description = $availablePaymentMethodInfo['tag'];

                        if (!$availablePaymentMethodInfo['logo']) {
                            $logo = 'https://cdn.myafterpay.com/logo/AfterPay_logo_checkout.svg';
                        } else $logo = $availablePaymentMethodInfo['logo'];

                        if (!$availablePaymentMethodInfo['installments']) {
                            $installments = '';
                        } else $installments = array_reverse($availablePaymentMethodInfo['installments']);

                        if (!$availablePaymentMethodInfo['dueAmount']) {
                            $dueAmount = '';
                        } else {
                            $dueAmount = $availablePaymentMethodInfo['dueAmount'];
                        }
                    }

                    if (!$availablePaymentMethodInfo['customerConsent']) {
                        $customerConsent = '';
                    } else $customerConsent = $availablePaymentMethodInfo['customerConsent'];

                    if (!$availablePaymentMethodInfo['termsUrl']) {
                        $termsUrl = '';
                    } else $termsUrl = $availablePaymentMethodInfo['termsUrl'];

                    if (!$availablePaymentMethodInfo['privacyUrl']) {
                        $privacyUrl = '';
                    } else $privacyUrl = $availablePaymentMethodInfo['privacyUrl'];

                    if (!$availablePaymentMethodInfo['termsText']) {
                        $termsText = '';
                    } else $termsText = $availablePaymentMethodInfo['termsText'];

                    $paymentMethods[] = [
                        'id' => $country . '_' . $afterpayPaymentMethod,
                        'country' => Tools::strtoupper($country),
                        'type' => $afterpayPaymentMethod,
                        'name' => $name,
                        'description' => $description,
                        'logo' => $logo,
                        'show_dob' => (Configuration::get($key . '_SHOW_DOB') == "1") ? true : false,
                        'show_phone' => (Configuration::get($key . '_SHOW_PHONE') == "1") ? true : false,
                        'show_iban' => (Configuration::get($key . '_SHOW_IBAN') == "1") ? true : false,
                        'show_company' => (Configuration::get($key . '_SHOW_COMPANY') == "1") ? true : false,
                        'show_coc' => (Configuration::get($key . '_SHOW_COC') == "1") ? true : false,
                        'show_ssn' => (Configuration::get($key . '_SHOW_SSN') == "1") ? true : false,
                        'ssn_placeholder' => Configuration::get($key . '_SSN_PLACEHOLDER'),
                        'ssn_maxlength' => Configuration::get($key . '_SSN_MAXLENGTH'),
                        'installments' => $installments,
                        'customerConsent' => $customerConsent == "1" ? true : false,
                        'termsUrl' => $termsUrl,
                        'privacyUrl' => $privacyUrl,
                        'termsText' => $termsText,
                        'dueAmount' => $dueAmount,
                    ];
                }
            }
        }

        // Only return payment methods of the current country
        $countries = Country::getCountries($this->context->language->id, true);
        $addressInvoice = new Address($cart->id_address_invoice);
        $addressDelivery = new Address($cart->id_address_delivery);

        // If the delivery country is not the same as the invoice country, then AfterPay is not allowed.
        if ($addressInvoice->id_country !== $addressDelivery->id_country) {
            return [];
        }

        // Check if the IP restriction is used.
        if (Configuration::hasKey('AFTERPAY_IP_RESTRICTION') && Configuration::get('AFTERPAY_IP_RESTRICTION') !== '') {
            $ip_configured = Configuration::get('AFTERPAY_IP_RESTRICTION');
            $ip_client = Tools::getRemoteAddr();
            if (strpos($ip_configured, $ip_client) === false) {
                return [];
            }
        }

        $countryCode = $countries[$addressInvoice->id_country]['iso_code'];

        foreach ($paymentMethods as $paymentMethodId => $paymentMethodValue) {
            if ($paymentMethodValue['country'] !== $countryCode) {
                unset($paymentMethods[$paymentMethodId]);
            }
        }

        if ($cart === null) {
            $this->paymentMethods = $paymentMethods;

            return $paymentMethods;
        }

        return $paymentMethods;
    }

    // Add payment method to terms,conditions and privacy terms
    private function getTermsandconditions($afterpayPaymentMethod)
    {

        if ($afterpayPaymentMethod == 'digitalinvoice') {
            return 'invoice';
        }
        if ($afterpayPaymentMethod == 'payin3') {
            return 'payin3';
        }
        if ($afterpayPaymentMethod == 'directdebit') {
            return 'direct_debit';
        }
        if ($afterpayPaymentMethod == 'installments') {
            return 'fix_installments';
        }
        if ($afterpayPaymentMethod == 'Campaign') {
            return 'campaign_invoice';
        }
        if ($afterpayPaymentMethod == 'Flex') {
            return 'part_payment';
        }
    }

    public function getProfileTrackingMode($paymentMethodId)
    {
        // profile tracking
        $trackingMode = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentMethodId) . '_PROFILE_TRACKING');
        switch ($trackingMode) {
            case 'Enabled-mandatory':
                $mode = 'mandatory';
                break;
            case 'Enabled-optional':
                $mode = 'optional';
                break;
            case 'Disabled':
                $mode = 'disabled';
                break;
            default:
                $mode = 'disabled';
        }
        return $mode;
    }

    private function getIntroductionText($paymentMethod)
    {

        // Adding introduction text to the payment form
        $dob = $paymentMethod['show_dob'];
        $phone = $paymentMethod['show_phone'];
        $iban = $paymentMethod['show_iban'];
        $company = $paymentMethod['show_company'];
        $ssn = $paymentMethod['show_ssn'];

        // create array with the admin panel configuration fields
        $formFields = array($dob, $phone, $iban, $company, $ssn);
        $language = $this->context->language->iso_code;

        $dobString = $this->l('date of birth');
        $phoneString = $this->l('phone number');
        $ibanString =  $this->l('bank account number');
        $companyString = $this->l('company name');
        $ssnString = $this->l('social security number');

        $counter = 0;
        
        for ($x = 0; $x < 5; $x++) {
            if ($formFields[$x] == true) {
                $counter++;
            }
        }

        // Only display introduction text when there are enabled fields
        if ($counter == 0) {
            $text = '';
        } else {
            $text = $this->l('In order to guarantee a secure checkout, we need to verify your ');
        }

        // Adding conditions to display enabled fields with punctuation marks 
        for ($i = 0; $i < $counter; $i++) {
            if ($counter != 1) {

                if ($i == $counter - 2) {
                    if ($dob == true) {
                        $text .= $dobString;
                        $dob = false;
                    } elseif ($phone == true) {
                        $text .= $phoneString;
                        $phone = false;
                    } elseif ($iban == true) {
                        $text .= $ibanString;
                        $iban = false;
                    } elseif ($company == true) {
                        $text .= $companyString;
                        $company = false;
                    } elseif ($ssn == true) {
                        $text .= $ssnString;
                        $ssn = false;
                    }
                    $text .= ' ';
                } elseif ($i != $counter - 1) {
                    if ($dob == true) {
                        $text .= $dobString;
                        $dob = false;
                    } elseif ($formFields[$phone] == true) {
                        $text .= $phoneString;
                        $phone = false;
                    } elseif ($iban == true) {
                        $text .= $ibanString;
                        $iban = false;
                    } elseif ($company == true) {
                        $text .= $companyString;
                        $company = false;
                    } elseif ($ssn == true) {
                        $text .= $ssnString;
                        $ssn = false;
                    }
                    $text .= ', ';
                } else {
                    if ($language == 'nl') {
                        $text .= 'en ';

                        if ($dob == true) {
                            $text .= $dobString;
                        } elseif ($phone == true) {
                            $text .= $phoneString;
                        } elseif ($iban == true) {
                            $text .= $ibanString;
                        } elseif ($company == true) {
                            $text .= $companyString;
                        } elseif ($ssn == true) {
                            $text .= $ssnString;
                        }

                        $text .= ' in te vullen.';
                    } else {
                        if ($language != 'nl') {
                            $text .= $this->l('and ');
                        }

                        if ($dob == true) {
                            $text .= $dobString;
                        } elseif ($phone == true) {
                            $text .= $phoneString;
                        } elseif ($iban == true) {
                            $text .= $ibanString;
                        } elseif ($company == true) {
                            $text .= $companyString;
                        } elseif ($ssn == true) {
                            $text .= $ssnString;
                        }
                        $text .= '.';
                    }
                }
            } else {
                if ($language == 'nl') {
                    if ($dob == true) {
                        $text .= $dobString;
                    } elseif ($phone == true) {
                        $text .= $phoneString;
                    } elseif ($iban == true) {
                        $text .= $ibanString;
                    } elseif ($company == true) {
                        $text .= $companyString;
                    } elseif ($ssn == true) {
                        $text .= $ssnString;
                    }
                    $text .= ' in te vullen.';
                } else {
                    if ($dob == true) {
                        $text .= $dobString;
                    } elseif ($phone == true) {
                        $text .= $phoneString;
                    } elseif ($iban == true) {
                        $text .= $ibanString;
                    } elseif ($company == true) {
                        $text .= $companyString;
                    } elseif ($ssn == true) {
                        $text .= $ssnString;
                    }
                    $text .= '.';
                }
            }
        }
        return $text;
    }

    /**
     * Function to get the AfterPay Digital Invoice form.
     *
     * @param string $paymentmethod
     * @return void
     */
    private function getDigitalInvoiceForm($paymentMethod)
    {
        $text = $this->getIntroductionText($paymentMethod);
        $tracking = $this->getProfileTrackingMode($paymentMethod['id']);

        $this->context->smarty->assign([
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'payment_option_id' => $paymentMethod['id'],
            'year' => date('Y') - 18,
            'lowest_year' => date('Y') - 100,
            'show_dob' => $paymentMethod['show_dob'],
            'show_phone' => $paymentMethod['show_phone'],
            'show_company' => $paymentMethod['show_company'],
            'show_coc' => $paymentMethod['show_coc'],
            'show_ssn' => $paymentMethod['show_ssn'],
            'ssn_placeholder' => $paymentMethod['ssn_placeholder'],
            'ssn_maxlength' => $paymentMethod['ssn_maxlength'],
            'text' => $text,
            'tracking' => $tracking,
            'customerConsent' => $paymentMethod['customerConsent'],
            'termsUrl' => $paymentMethod['termsUrl'],
            'privacyUrl' => $paymentMethod['privacyUrl']
        ]);

        // Display payment form with mandatory terms and conditions for Austria, Switzerland and Germany
        if ($paymentMethod['country'] == 'AT' || $paymentMethod['country'] == 'CH' || $paymentMethod['country'] == 'DE') {
            return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_mandatoryterms_digitalinvoice.tpl');
        } else
            return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_digitalinvoice.tpl');
    }

    /**
     * Function to get the AfterPay pay in 3 form.
     *
     * @param string $paymentmethod
     * @return void
     */
    private function getPayIn3Form($paymentMethod)
    {
        $text = $this->getIntroductionText($paymentMethod);
        $tracking = $this->getProfileTrackingMode($paymentMethod['id']);
        $termsTextFlag = $paymentMethod['termsText'] ? true : false;

        $this->context->smarty->assign([
            'name' => $paymentMethod['name'],
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'payment_option_id' => $paymentMethod['id'],
            'year' => date('Y') - 18,
            'lowest_year' => date('Y') - 100,
            'show_dob' => $paymentMethod['show_dob'],
            'show_phone' => $paymentMethod['show_phone'],
            'show_company' => $paymentMethod['show_company'],
            'show_coc' => $paymentMethod['show_coc'],
            'show_ssn' => $paymentMethod['show_ssn'],
            'ssn_placeholder' => $paymentMethod['ssn_placeholder'],
            'ssn_maxlength' => $paymentMethod['ssn_maxlength'],
            'text' => $text,
            'tracking' => $tracking,
            'customerConsent' => $paymentMethod['customerConsent'],
            'termsTextFlag' => $termsTextFlag,
            'termsUrl' => $paymentMethod['termsText'],
            'privacyUrl' => $paymentMethod['privacyUrl'],
            'termsText' => $paymentMethod['termsText'],
            'dueAmount' => $paymentMethod['dueAmount'],
            'PayinxElement' => true,
            'scriptModule' => 'https://cdn.bnpl.riverty.io/elements/v1/build/riverty-elements.esm.js',
            'scriptNoModule' => 'https://cdn.bnpl.riverty.io/elements/v1/build/riverty-elements.js',
            'payInxAmount' => (float)$this->context->cart->getOrderTotal(true, Cart::BOTH),
        ]);

        // Display payment form with terms and conditions for Netherlands
        return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_payin3.tpl');
    }

    /**
     * Function to get the AfterPay Direct Debit form.
     *
     * @param string $paymentmethod
     * @return void
     */
    private function getDirectDebitForm($paymentMethod)
    {
        $text = $this->getIntroductionText($paymentMethod);
        $tracking = $this->getProfileTrackingMode($paymentMethod['id']);

        $this->context->smarty->assign([
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'payment_option_id' => $paymentMethod['id'],
            'year' => date('Y') - 18,
            'lowest_year' => date('Y') - 100,
            'show_dob' => $paymentMethod['show_dob'],
            'show_phone' => $paymentMethod['show_phone'],
            'show_iban' => $paymentMethod['show_iban'],
            'text' => $text,
            'tracking' => $tracking,
            'customerConsent' => $paymentMethod['customerConsent'],
            'termsUrl' => $paymentMethod['termsUrl'],
            'privacyUrl' => $paymentMethod['privacyUrl']
        ]);

        // Display payment form with mandatory terms and conditions for Austria, Switzerland and Germany
        if ($paymentMethod['country'] == 'AT' || $paymentMethod['country'] == 'CH' || $paymentMethod['country'] == 'DE') {
            return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_mandatoryterms_directdebit.tpl');
        } else
            return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_directdebit.tpl');
    }

    private function getInstallmentsForm($paymentMethod)
    {
        $text = $this->getIntroductionText($paymentMethod);
        $tracking = $this->getProfileTrackingMode($paymentMethod['id']);

        $this->context->smarty->assign([
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'payment_option_id' => $paymentMethod['id'],
            'year' => date('Y') - 18,
            'lowest_year' => date('Y') - 100,
            'show_dob' => $paymentMethod['show_dob'],
            'show_phone' => $paymentMethod['show_phone'],
            'show_iban' => $paymentMethod['show_iban'],
            'text' => $text,
            'tracking' => $tracking,
            'installments' => $paymentMethod['installments'],
            'customerConsent' => $paymentMethod['customerConsent'],
            'termsUrl' => $paymentMethod['termsUrl'],
            'privacyUrl' => $paymentMethod['privacyUrl']
        ]);

        // Display payment form with mandatory terms and conditions for Austria and Germany
        return $this->context->smarty->fetch('module:riverty/views/templates/front/payment_form_mandatoryterms_installments.tpl');
    }

    /**
     * Function to check and / or add an order state.
     *
     * @param string $name
     * @return boolean
     */
    public function addOrderState($name)
    {
        $stateExist = false;
        $states = OrderState::getOrderStates((int)$this->context->language->id);

        // check if order state exist
        foreach ($states as $state) {
            if (in_array($name, $state)) {
                $stateExist = true;
                break;
            }
        }

        // If the state does not exist, we create it.
        if (!$stateExist) {
            // create new order state
            $orderState = new OrderState();
            $orderState->color = '#005e51';
            $orderState->delivery = false;
            $orderState->send_email = false;
            $orderState->module_name = 'riverty';
            $orderState->template = '';
            $orderState->name = array();
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                $orderState->name[$language['id_lang']] = $name;
            }

            // Update object
            if ($orderState->add()) {
                $this->createOrderStateLogo($orderState->id);
            }
        }

        return true;
    }

    /**
     * Function to copy the AfterPay logo to the Prestashop order state image folder.
     *
     * @param int $orderStateId
     * @return void
     */
    public function createOrderStateLogo($orderStateId)
    {
        $source = _PS_MODULE_DIR_ . 'riverty/views/img/logo_small.png';
        $destination = _PS_ORDER_STATE_IMG_DIR_ . $orderStateId . '.gif';
        @copy($source, $destination);
    }

    /**
     * Function to get the ID of an order state.
     *
     * @param string $name
     * @return int
     */
    public function getOrderStateId($name)
    {
        $states = OrderState::getOrderStates((int)$this->context->language->id);

        // check if order state exist
        foreach ($states as $state) {
            if (in_array($name, $state)) {
                return $state['id_order_state'];
            }
        }
    }

    /**
     * Function to change the state of an order.
     *
     * @access  public
     * @param   int     $currentOrder
     * @param   string  $state
     * @return  void
     */
    public function changeOrderState($currentOrder, $state)
    {
        $this->module->writeLog('Start change order state. Order: ' . $currentOrder . ' to: ' . $state);

        // Get the order history to set new order status.
        $history = new OrderHistory();

        //$history->id_order = (int) $this->module->currentOrder;
        $history->id_order = (int) $currentOrder;

        // Then set order to AfterPay payment accepted state.
        $paymentStatus = $this->module->getOrderStateId($state);

        //@todo check if the order state is good
        $history->changeIdOrderState($paymentStatus, (int)($currentOrder));
        $history->add();

        $this->module->writeLog('Changed order state');
        //@todo check if history can be added
    }

    /**
     * Function to get the configured connection mode for the payment method.
     *
     * @access public
     * @param resource $afterpay The afterpay object.
     * @return void
     **/
    public function getConnectionMode($paymentMethodId)
    {
        $connectionMode = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentMethodId) . '_TESTMODE');
        switch ($connectionMode) {
            case 'test':
            default:
                $mode = 'test';
                break;
            case 'production':
                $mode = 'production';
                break;
            case 'sandbox':
                $mode = 'sandbox';
                break;
        }
        return $mode;
    }

    /**
     * Function to send AfterPay debug email using the AfterPay Library debuglog function
     *
     * @access public
     * @param resource $afterpay The afterpay object.
     * @return void
     **/
    public function sendAfterpayDebugMail($afterpay)
    {
        if (Configuration::hasKey('AFTERPAY_DEBUG_EMAIL') && Configuration::get('AFTERPAY_DEBUG_EMAIL') !== '') {
            $debugEmailAddress = Configuration::get('AFTERPAY_DEBUG_EMAIL');
            Mail::Send(
                (int)(Configuration::get('PS_LANG_DEFAULT')),
                'debug',
                'Afterpay Debug Email',
                array(
                    '{email}' => Configuration::get('PS_SHOP_EMAIL'),
                    '{message}' => $afterpay->client->getDebugLog()
                ),
                $debugEmailAddress,
                null,
                null,
                null,
                null,
                null,
                $this->getLocalPath() . 'mails/'
            );
        }
    }

    /**
     * Function to add message to order.
     *
     * @access public
     * @param resource $order The prestashop order object.
     * @param resource $message The private message that should be added to the order.
     * @return void
     **/
    public function addOrderMessage($order, $message)
    {
        // Add this message in the customer thread
        $customer_thread = new CustomerThread();
        $customer_thread->id_contact = 0;
        $customer_thread->id_customer = (int) $order->id_customer;
        $customer_thread->id_shop = (int) $this->context->shop->id;
        $customer_thread->id_order = (int) $order->id;
        $customer_thread->id_lang = (int) $this->context->language->id;
        $customer_thread->email = 'noreply@afterpay.nl';
        $customer_thread->status = 'closed';
        $customer_thread->token = Tools::passwdGen(12);
        $customer_thread->add();

        $customer_message = new CustomerMessage();
        $customer_message->id_customer_thread = $customer_thread->id;
        $customer_message->id_employee = 0;
        $customer_message->message = $message;
        $customer_message->private = 1;
        $customer_message->add();
        // @todo: check if message could be added.
    }

    /**
     * Function to get the AfterPay NL / BE Tax Category based on full amount and tax amount
     *
     * @access public
     * @param  float $total_amount Total amount.
     * @param  float $tax_amount Tax amount.
     * @return int $item_tax_category
     **/
    public function getAfterpayTaxClass($totalAmount, $taxAmount)
    {
        // We manually calculate the tax percentage here.
        if ($taxAmount > 0) {
            // Calculate tax percentage.
            $itemTaxPercentage = number_format(($taxAmount / $totalAmount) * 100, 2, '.', '');
        } else {
            $itemTaxPercentage = 0.00;
        }
        if ($itemTaxPercentage > 10) {
            $itemTaxCategory = 1;
        } elseif ($itemTaxPercentage > 0) {
            $itemTaxCategory = 2;
        } else {
            $itemTaxCategory = 3;
        }
        return $itemTaxCategory;
    }

    /**
     * Function to hook into the refund process of Prestashop and process to AfterPay.
     *
     * @access public
     * @param  array $params Refund params.
     * @return boolean
     **/
    public function hookActionProductCancel($params)
    {
        // First check if order is paid with AfterPay.
        $order = $params['order'];

        if ($order->module !== 'riverty') {
            return;
        }

        // Check if refunding is enabled.
        $useRefunds = Configuration::get('AFTERPAY_USE_REFUNDS');
        if (!$useRefunds) {
            return;
        }

        $this->writeLog('Start AfterPay Refund');

        // Get the order information.
        $orderlines = [];
        $orderNumber = $order->reference;
        $invoiceNumber = '';

        // Get the payment information.
        $orderPaymentArray = $order->getOrderPayments();
        if (empty($orderPaymentArray)) {
            $error = 'Error refunding order: payment information cannot be found';
            $this->module->writeLog($error);
            return false;
        }
        $orderPayment = reset($orderPaymentArray);
        $paymentMethod = $orderPayment->payment_method;

        // Get the order history to set new order status.
        $history = new OrderHistory();
        $history->id_order = $order->id;

        // Get the currency of the order
        $currency = new Currency($order->id_currency);
        $currencyCode = $currency->iso_code;

        if ($order->hasBeenShipped()) {
            // this is a 'return product' action
            $this->writeLog('This is a "return product" action');
            $this->writeLog('Do a refund for payment method: ' . $paymentMethod);

            // Get the invoice information
            $invoiceNumber = $order->invoice_number;
            $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
            $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
            $this->writeLog('Invoicenumber: ' . $invoiceNumberFormatted);
        } elseif ($order->hasBeenPaid()) {
            // this is a 'standard refund'
            $this->writeLog('This is a "standard refund"');
            $this->writeLog('Do a refund for payment method: ' . $paymentMethod);

            // Get the invoice information
            $invoiceNumber = $order->invoice_number;
            $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
            $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
            $this->writeLog('Invoicenumber: ' . $invoiceNumberFormatted);
        } else {
            return;
        }

        // Check if a refund action form is posted.
        if (Tools::getIsset('cancel_product')) {
            $cancelProduct = Tools::getValue('cancel_product');
            foreach ($cancelProduct as $key => $value) {
                if (strpos($key, 'quantity') !== false && $value > 0) {
                    $productId = Tools::substr($key, strpos($key, "_") + 1);
                    $orderDetail = new OrderDetail((int) $productId);
                    $orderlines[$productId]['qty'] = $cancelProduct['quantity_' . $productId];
                    $orderlines[$productId]['netUnitPrice'] = $orderDetail->unit_price_tax_excl;
                    $orderlines[$productId]['grossUnitPrice'] = $cancelProduct['amount_' . $productId] / $orderlines[$productId]['qty'];
                    $orderlines[$productId]['vatAmount'] = round(
                        ($orderlines[$productId]['grossUnitPrice'] - $orderlines[$productId]['netUnitPrice']),
                        4
                    );
                    $orderlines[$productId]['sku'] = $orderDetail->product_reference;
                    $orderlines[$productId]['name'] = $orderDetail->product_name;
                }
                if ($key == 'shipping_amount' && $value != 0) {
                    $orderlines['shipping']['sku'] = 'SHIPPING';
                    $orderlines['shipping']['qty'] = 1;
                    $orderlines['shipping']['netUnitPrice'] = $value;
                    $orderlines['shipping']['grossUnitPrice'] = $value;
                    $orderlines['shipping']['vatAmount'] = 0;
                    $orderlines['shipping']['name'] = 'Shipping';
                }
                if ($key == 'voucher_refund_type' && $value == 1 && $order->total_discounts_tax_incl > 0) {
                    $orderlines['discount']['sku'] = 'DISCOUNT';
                    $orderlines['discount']['qty'] = 1;
                    $orderlines['discount']['netUnitPrice'] = $order->total_discounts_tax_excl * -1;
                    $orderlines['discount']['grossUnitPrice'] = $order->total_discounts_tax_incl * -1;
                    $orderlines['discount']['vatAmount'] = round(
                        ($order->total_discounts_tax_incl - $order->total_discounts_tax_excl),
                        4
                    ) * -1;
                    $orderlines['discount']['name'] = 'Discount';
                }
            }
        }
        //send the get_order request and retrieve the balance
        $orderResult = $this->getOderDetails($paymentMethod, $orderNumber);
        $capturesArray = $orderResult->return->captures[0];
        $balanceAmount = $capturesArray->balance;

        //check whether this order is refundable or not
        if ($balanceAmount > 0) {
            $result = $this->requestRefund($paymentMethod, $orderNumber, $invoiceNumberFormatted, $orderlines);

            if (isset($result->return->resultId) && $result->return->resultId == 0) {
                if (isset($result->return->totalCapturedAmount) && isset($result->return->totalRefundedAmount)) {
                    $totalCapturedAmount = $result->return->totalCapturedAmount;
                    $totalRefundedAmount = $result->return->totalRefundedAmount;
                    if (($totalCapturedAmount - $totalRefundedAmount) == 0) {
                        $this->writeLog('Refund was a full refund');
                        $paymentStatus = $this->getOrderStateId('Riverty - Fully refunded');
                        $message = 'Riverty - Fully refunded: ' . $totalRefundedAmount . ' '
                            . $currencyCode . ' (incl. tax.)';
                        $this->addOrderMessage($order, $message);
                        $history->changeIdOrderState($paymentStatus, $order->id);
                        $history->add();
                    } else {
                        $this->writeLog('Refund was a partial refund');
                        $paymentStatus = $this->getOrderStateId('Riverty - Partially refunded');
                        $message = 'Riverty - Partially refunded: ' . $totalRefundedAmount . ' '
                            . $currencyCode . ' (incl. tax.)';
                        $this->addOrderMessage($order, $message);
                        $history->changeIdOrderState($paymentStatus, $order->id);
                        $history->add();
                    }
                }
                $this->writeLog('Refund was successful');
                return true;
            } else {
                $errorText = 'There is a problem with the refund to Riverty';
                if (isset($result->return->failures->description)) {
                    $errorText .= ': ' . $result->return->failures->description;
                }
                $redirectLink = Context::getContext()->link->getAdminLink(
                    'AdminOrders',
                    true,
                    [],
                    [
                        'id_customer' => $order->id_customer,
                        'viewcustomer' => 1
                    ]
                )  . "&id_order=" . $order->id . '&vieworder';
                $this->writeLog($errorText);
                $this->get('session')->getFlashBag()->add('error', $errorText);
                Tools::redirectAdmin($redirectLink);
            }
        }
    }

    /**
     * Function to hook into the change of a status of an order.
     *
     * @access public
     * @param  array $params Refund params.
     * @return boolean
     **/
    public function hookActionOrderStatusUpdate($params)
    {
        // Check if order is an order paid with AfterPay.
        if (!isset($params['id_order'])) {
            return;
        } else {
            $order = new Order($params['id_order']);
            if ($order->module !== 'riverty') {
                return;
            }
        }

        // Get the status on which the capture should take place.
        $captureStatus = Configuration::get('AFTERPAY_CAPTURE_STATUS');
        $currentStatus = $params['newOrderStatus']->id;

        // If the capture status is null, or not the same as the status on which a capture should take place.
        if ($captureStatus == null || $captureStatus != $currentStatus) {
            return;
        }

        // Get the payment information.
        $orderPaymentArray = $order->getOrderPayments();
        if (empty($orderPaymentArray)) {
            $error = 'Error refunding order: payment information cannot be found';
            $this->module->writeLog($error);
            return false;
        }
        $orderPayment = reset($orderPaymentArray);
        $paymentMethod = $orderPayment->payment_method;

        // Get the order history to set new order status.
        $history = new OrderHistory();
        $history->id_order = $order->id;

        // Check if the order should be captured.
        $useCaptures = Configuration::get('AFTERPAY_USE_CAPTURES');
        if ($useCaptures == 'yes-onstatus') {
            $invoiceNumber = $order->invoice_number;
            if ($invoiceNumber == 0) {
                // Order is not yet invoice, so cannot be captured yet.
                return;
            }
            $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
            $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
            $captureResult = $this->requestCapture($paymentMethod, $order->id, $invoiceNumberFormatted);
            if ($captureResult === true) {
                $paymentStatus = $this->getOrderStateId('Riverty - Captured');
                $history->changeIdOrderState($paymentStatus, $order->id);
                $history->add();
            } else {
                $paymentStatus = $this->getOrderStateId('Payment error');
                $history->changeIdOrderState($paymentStatus, $order->id);
                $history->add();
            }
        }

        $this->writeLog(json_encode($params, JSON_PRETTY_PRINT));
    }

    /**
     * Function to execute a capture request towards the AfterPay API.
     *
     * @access private
     * @param  string $paymentMethod Payment method code.
     * @param  string $orderId       Order Id.
     * @param  string $invoiceId     Invoice Id
     * @return object
     **/
    private function requestCapture($paymentMethod, $orderId, $invoiceId)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('capture_full');

        // Get connection mode.
        $paymentMethod = str_replace('afterpay_', '', Tools::strtolower($paymentMethod));
        $afterpayConnectionMode = $this->getConnectionMode($paymentMethod);

        // Get authorization key.
        $authorisation = array();
        $authorisation['apiKey'] = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentMethod) . '_' . Tools::strtoupper($afterpayConnectionMode) . '_API_KEY');

        $order = new Order($orderId);

        $afterpayOrder = array();
        $afterpayOrder['ordernumber'] = $order->reference;
        $afterpayOrder['invoicenumber'] = $invoiceId;
        $afterpayOrder['totalamount'] = round($order->total_paid_tax_incl * 100, 0);
        $afterpayOrder['totalNetAmount'] = $order->total_paid_tax_excl;

        $this->writeLog('Start capture action for order: ' . $afterpayOrder['ordernumber']);

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order($afterpayOrder, 'OM');
            $this->writeLog('Capture request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
            $afterpay->do_request($authorisation, $afterpayConnectionMode);
            $this->writeLog('Capture result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
            $this->sendAfterpayDebugMail($afterpay);

            // Retreive response.
            if (isset($afterpay->order_result->return->resultId)) {
                switch ($afterpay->order_result->return->resultId) {
                    case 0:
                        // Capture was succesfull.
                        $this->writeLog('Capture was succesfull');
                        return true;
                    case 1:
                        // Capture failed.
                        $this->writeLog('Capture failed');
                        return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->writeLog('Exception during capturing order: ' . $e->getMessage());
            return false;
        }
    }

    /**
     * Function to execute a refund request towards the AfterPay API.
     *
     * @access private
     * @param  string $paymentmethod Payment method code.
     * @param  string $orderNumber   Order number.
     * @param  string $invoiceNumber Invoice number
     * @param  array  $orderlines    Order lines to refund.
     * @return object
     **/
    private function requestRefund($paymentmethod, $orderNumber, $invoiceNumber, $orderlines)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('refund_partial');

        // Set up the additional information
        $aporder = array();
        $aporder['invoicenumber'] = $invoiceNumber;
        $aporder['ordernumber'] = $orderNumber;

        // Set up order lines, repeat for more order lines
        foreach ($orderlines as $orderline) {
            $afterpay->create_order_line(
                $orderline['sku'],
                $orderline['name'],
                $orderline['qty'],
                ($orderline['grossUnitPrice'] * 100) * -1,
                null,
                $orderline['vatAmount'] * -1
            );
        }

        // Create the order object for order management (OM)
        $afterpay->set_order($aporder, 'OM');

        // Get connection mode.
        $paymentmethod = str_replace('afterpay_', '', Tools::strtolower($paymentmethod));
        $afterpayConnectionMode = $this->getConnectionMode($paymentmethod);

        // Get authorization key.
        $authorisation = array();
        $authorisation['apiKey'] = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentmethod) . '_' . Tools::strtoupper($afterpayConnectionMode) . '_API_KEY');

        $this->writeLog('Refund request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
        $afterpay->do_request($authorisation, $afterpayConnectionMode);
        $this->writeLog('Refund result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
        return $afterpay->order_result;
    }

    /**
     * Function to execute a refund request towards the AfterPay API.
     * @access private
     * @param  string $paymentmethod Payment method code.
     * @param  string $orderNumber   Order number.
     * 
     * @return object
     */
    private function getOderDetails($paymentmethod, $orderNumber)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('get_order');

        // Set up the additional information
        $aporder = array();
        $aporder['ordernumber'] = $orderNumber;

        // Create the order object for order management (OM)
        $afterpay->set_order($aporder, 'OM');

        // Get connection mode.
        $paymentmethod = str_replace('afterpay_', '', Tools::strtolower($paymentmethod));
        $afterpayConnectionMode = $this->getConnectionMode($paymentmethod);

        // Get authorization key.
        $authorisation = array();
        $authorisation['apiKey'] = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentmethod) . '_' . Tools::strtoupper($afterpayConnectionMode) . '_API_KEY');

        $this->writeLog('getOrder request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
        $afterpay->do_request($authorisation, $afterpayConnectionMode);
        $this->writeLog('getOrder result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
        return $afterpay->order_result;
    }

    /**
     * Function to fetch available payment methods on AfterPay API.
     *
     * @access private
     * @param  float  $orderTotal            Order total amount.
     * @param  string $paymentMethodId       Internal payment method identifier.
     * @param  string $afterpayPaymentMethod External payment method name.
     * @return array
     **/
    private function getAvailablePaymentMethod($orderTotal, $paymentMethodId, $afterpayPaymentMethod, $cart)
    {
        $type = '';
        $title = '';
        $tag = '';
        $logo = '';
        $termsText = '';

        $currency = new Currency($cart->id_currency);
        $currencyCode = $currency->iso_code;
        $language = $this->context->language->iso_code;
        $totalGrossAmount = $orderTotal;
        $totalNetAmount = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);

        // Adding country to available payment methods request
        $countryCode = ['AT_', 'BE_', 'DE_', 'NL_', 'CH_', 'NO_', 'SE_', 'FI_', 'DK_'];

        foreach ($countryCode as $code) {
            if (strstr($paymentMethodId, $code)) {
                $country = $code;
            }
        }

        try {
            $afterpay = new Afterpay\Afterpay();
            $afterpay->setRest(true);
            $afterpay->set_ordermanagement('available_payment_methods');

            $order = [
                'order' => [
                    'totalGrossAmount' => $orderTotal,
                    'totalNetAmount' => $totalNetAmount,
                    'currency' => $currencyCode
                ],
                'country'  => trim($country, '_'),
                'conversationLanguage' => $language,
                'additionalData' => [
                    'pluginProvider' => $this->author,
                    'pluginVersion' => $this->version,
                    'shopUrl' => Tools::getHttpHost(true) . Configuration::get('PS_BASE_URL'),
                    'shopPlatform' => 'Prestashop',
                    'shopPlatformVersion' => Configuration::get('PS_INSTALL_VERSION')
                ],
            ];
            $afterpay->set_order($order, 'OM');

            // Get connection mode.
            $afterpayConnectionMode = $this->getConnectionMode($paymentMethodId);

            // Get authorization key.
            $authorisation = [];
            $authorisation['apiKey'] = Configuration::get(
                'AFTERPAY_' . $paymentMethodId . '_' . Tools::strtoupper($afterpayConnectionMode) . '_API_KEY'
            );

            $afterpay->do_request($authorisation, $afterpayConnectionMode);

            if (isset($afterpay->order_result->return) && $afterpay->order_result->return->resultId == 0) {
                if (
                    isset($afterpay->order_result->return->paymentMethods)
                    && count($afterpay->order_result->return->paymentMethods) > 0
                ) {
                    foreach ($afterpay->order_result->return->paymentMethods as $returnedPaymentMethod) {
                        if (
                            Tools::strtoupper($afterpayPaymentMethod) == 'DIGITALINVOICE'
                            && $returnedPaymentMethod->type == 'Invoice'
                            && !isset($returnedPaymentMethod->directDebit)
                        ) {
                            $type = 'Invoice';
                            $title = $returnedPaymentMethod->title;
                            $tag = $returnedPaymentMethod->tag;
                            $logo = $returnedPaymentMethod->logo;
                            $customerConsent = $returnedPaymentMethod->legalInfo->requiresCustomerConsent;
                            $termsUrl = $returnedPaymentMethod->legalInfo->termsAndConditionsUrl;
                            $privacyUrl = $returnedPaymentMethod->legalInfo->privacyStatementUrl;
                        }
                        $dueAmount = 0 ;
                        if (
                            Tools::strtoupper($afterpayPaymentMethod) == 'PAYIN3'
                            && $returnedPaymentMethod->type == 'PayinX'
                            && !isset($returnedPaymentMethod->directDebit)
                        ) {
                            $type = 'PayinX';
                            $title = $returnedPaymentMethod->title;
                            $tag = $returnedPaymentMethod->tag;
                            $logo = $returnedPaymentMethod->logo;
                            $dueAmount = $returnedPaymentMethod->payInX[0]->dueAmount;
                            $customerConsent = $returnedPaymentMethod->legalInfo->requiresCustomerConsent;
                            $termsUrl = $returnedPaymentMethod->legalInfo->termsAndConditionsUrl;
                            $privacyUrl = $returnedPaymentMethod->legalInfo->privacyStatementUrl;
                            $termsText = $returnedPaymentMethod->legalInfo->text;
                        }
                        if (
                            Tools::strtoupper($afterpayPaymentMethod) == 'DIRECTDEBIT'
                            && $returnedPaymentMethod->type == 'Invoice'
                            && isset($returnedPaymentMethod->directDebit)
                        ) {
                            $type = 'Direct Debit';
                            $title = $returnedPaymentMethod->title;
                            $tag = $returnedPaymentMethod->tag;
                            $logo = $returnedPaymentMethod->logo;
                            $customerConsent = $returnedPaymentMethod->legalInfo->requiresCustomerConsent;
                            $termsUrl = $returnedPaymentMethod->legalInfo->termsAndConditionsUrl;
                            $privacyUrl = $returnedPaymentMethod->legalInfo->privacyStatementUrl;
                        }
                        
                        if (
                            Tools::strtoupper($afterpayPaymentMethod) == 'INSTALLMENTS'
                            && $returnedPaymentMethod->type == 'Installment'
                            && isset($returnedPaymentMethod->installment)
                        ) {
                            $installments[] = [
                                $type = 'Installment',
                                $title = $returnedPaymentMethod->title,
                                $tag = $returnedPaymentMethod->tag,
                                $logo = $returnedPaymentMethod->logo,
                                $basketAmount = $returnedPaymentMethod->installment->basketAmount,
                                $numberOfInstallments = $returnedPaymentMethod->installment->numberOfInstallments,
                                $installmentAmount = $returnedPaymentMethod->installment->installmentAmount,
                                $firstInstallmentAmount = $returnedPaymentMethod->installment->firstInstallmentAmount,
                                $lastInstallmentAmount = $returnedPaymentMethod->installment->lastInstallmentAmount,
                                $interestRate = $returnedPaymentMethod->installment->interestRate,
                                $effectiveInterestRate = $returnedPaymentMethod->installment->effectiveInterestRate,
                                $effectiveAnnualPercentageRate = $returnedPaymentMethod->installment->effectiveAnnualPercentageRate,
                                $totalInterestAmount = $returnedPaymentMethod->installment->totalInterestAmount,
                                $startupFee = $returnedPaymentMethod->installment->startupFee,
                                $monthlyFee = $returnedPaymentMethod->installment->monthlyFee,
                                $totalAmount = $returnedPaymentMethod->installment->totalAmount,
                                $installmentProfileNumber = $returnedPaymentMethod->installment->installmentProfileNumber,
                                $readMore = $returnedPaymentMethod->installment->readMore,
                                $customerConsent = $returnedPaymentMethod->legalInfo->requiresCustomerConsent,
                                $termsUrl = $returnedPaymentMethod->legalInfo->termsAndConditionsUrl,
                                $privacyUrl = $returnedPaymentMethod->legalInfo->privacyStatementUrl
                            ];
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->module->writeLog('Exception while trying to fetch available payment methods: ' . $e->getMessage());
        }

        $return = [
            'type' => $type,
            'title' => $title,
            'tag' => $tag,
            'logo' => $logo,
            'customerConsent' => $customerConsent,
            'termsUrl' => $termsUrl,
            'privacyUrl' => $privacyUrl,
            'termsText' => $termsText,
            'installments' => $installments,
            'dueAmount' => $dueAmount,
        ];
        return $return;
    }
    /**
     * Helper displaying confirmation message.
     *
     * @param string $string
     *
     * @return string
     */
    public function displayCustomConfirmation($string)
    {
        $output = '
        <div class="bootstrap" style="position:absolute; z-index:1000; width:100%">
        <div class="module_confirmation conf confirm alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            ' . $string . '
        </div>
        </div>';

        return $output;
    }

    /**
     * @param string $paymentmethod
     * @param int $orderNumber
     * @param string $afterpayConnectionMode
     * @param string $afterpayAPikey
     * 
     * @return array
     */
    public function getSCAOderDetails($paymentmethod, $orderNumber, $afterpayConnectionMode, $afterpayAPikey)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('get_order');

        // Set up the additional information
        $aporder = array();
        $aporder['ordernumber'] = $orderNumber;

        // Create the order object for order management (OM)
        $afterpay->set_order($aporder, 'OM');

        // Get authorization key.
        $authorisation = array();
        $authorisation['apiKey'] = $afterpayAPikey;

        $this->writeLog('getOrder request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
        $afterpay->do_request($authorisation, $afterpayConnectionMode);
        $this->writeLog('getOrder result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
        return $afterpay->order_result;
    }

    /**
     * @param int $orderId
     * @param int $invoiceId
     * @param string $afterpayConnectionMode
     * @param string $afterpayAPikey
     * @param array $afterpayOrderlines
     * @param array $afterpay_order
     * 
     * @return bool
     */
    public function redirectCaptureOrder($orderId, $invoiceId, $afterpayConnectionMode, $afterpayAPikey, $afterpayOrderlines, $afterpayOrder, $afterPayOrderNumber = null)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('capture_partial');

        // Get authorization key.
        $authorisation = [];
        $authorisation['apiKey'] = $afterpayAPikey;
        
        // Get order lines
        if (count($afterpayOrderlines) > 0) {
            foreach ($afterpayOrderlines as $orderline) {
                $afterpay->create_order_line(
                    $orderline['sku'],
                    $orderline['name'],
                    $orderline['qty'],
                    $orderline['price'],
                    null,
                    $orderline['vat_amount'],
                    null,
                    null,
                    $orderline['productUrl'],
                    $orderline['imageUrl'],
                    null
                );
            }
        }

        $order = new Order($orderId);

        $afterpayOrder = $afterpayOrder;
        $afterpayOrder['ordernumber'] = $order->reference;
        if ($afterpayOrder['ordernumber'] == null && $afterPayOrderNumber != null) {
            $afterpayOrder['ordernumber'] = $afterPayOrderNumber;
        }
        $afterpayOrder['invoicenumber'] = $invoiceId;

        $this->writeLog('Start capture action for order: ' . $afterpayOrder['ordernumber']);

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order($afterpayOrder, 'OM');
            $this->writeLog('Capture request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
            $afterpay->do_request($authorisation, $afterpayConnectionMode);
            $this->writeLog('Capture result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
            $this->sendAfterpayDebugMail($afterpay);

            // Retreive response.
            if (isset($afterpay->order_result->return->resultId)) {
                switch ($afterpay->order_result->return->resultId) {
                    case 0:
                        // Capture was succesfull.
                        $this->writeLog('Capture was succesfull');
                        return true;
                    case 1:
                        // Capture failed.
                        $this->writeLog('Capture failed');
                        return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->writeLog('Exception during capturing order: ' . $e->getMessage());
            return false;
        }
    }

    /**
     * @return string
     */
    private function getServerCronUrl()
    {
        $token = Configuration::get('rivertyCronToken');
        $phpPath = '* * * * * ' . shell_exec('which php');
        $cronUrl = $phpPath . ' ' . '"' . _PS_ROOT_DIR_ . DIRECTORY_SEPARATOR . 'index.php"';
        $cronUrl .= ' "fc=module&module=riverty&controller=customscacron&token=' . $token . '"';
        return $cronUrl;
    }

    /**
     * @return string
     */
    private function getWebServerCronUrl()
    {
        $cronUrl = Context::getContext()->link->getModuleLink('riverty', 'customscacron', array('token' => Configuration::get('rivertyCronToken')));
        return $cronUrl;
    }
}
