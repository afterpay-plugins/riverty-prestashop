<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{riverty}prestashop>payment_form_mandatoryterms_directdebit_c7801720c073d4f07714626104f5409c'] = 'Les conditions générales du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_mandatoryterms_directdebit_c5cf955f73c79c73f173c60124167a86'] = 'La politique de confidentialité du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_directdebit_c7801720c073d4f07714626104f5409c'] = 'Les conditions générales du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_directdebit_c5cf955f73c79c73f173c60124167a86'] = 'La politique de confidentialité du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_digitalinvoice_c7801720c073d4f07714626104f5409c'] = 'Les conditions générales du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_digitalinvoice_c5cf955f73c79c73f173c60124167a86'] = 'La politique de confidentialité du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_mandatoryterms_digitalinvoice_c7801720c073d4f07714626104f5409c'] = 'Les conditions générales du mode de paiement Riverty';
$_MODULE['<{riverty}prestashop>payment_form_mandatoryterms_digitalinvoice_c5cf955f73c79c73f173c60124167a86'] = 'La politique de confidentialité du mode de paiement Riverty';
