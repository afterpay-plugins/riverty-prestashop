{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<div class="row afterpay-header">
		<h1>Norway</h1>
	</div>

	<hr />

	<div class="alert alert-info" role="alert">
        <span>
            Do you want to offer this or any other Riverty payment method? 
			<a href="https://www.riverty.com/en/business/products/get-started-riverty-buy-now-pay-later/" target="_blank" class="alert-link">
			Sign up here </a> 
			or contact Riverty at: <a href="https://www.riverty.com/en/business/support/contact-us/" target="_blank" class="alert-link">
			https://www.riverty.com/en/business/support/contact-us/ </a>
        </span>
    </div> 

	<div class="afterpay-content">
		<div class="row">
			<div class="col-md-12">
				{html_entity_decode($form_settings_no_digitalinvoice|escape:'htmlall':'UTF-8')}
			</div>
		</div>
	</div>
</div>
