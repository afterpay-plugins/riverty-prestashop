{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <p>
                <img src="https://cdn.riverty.design/logo/riverty-logo-dark.svg" alt="Riverty logo" class="img-responsive" style="width:35%"/>
            </p>
            <p>
                <br>
                <strong>Offer your customers more flexible ways to pay in order to boost your sales.<p><a href="https://www.riverty.com/en/business/products/get-started-riverty-buy-now-pay-later/" target="_blank"> Get started with Riverty Buy Now Pay Later.</a></p></strong>
            </p>
            <p stye="text-align: justify; text-justify: inter-word;">
                With a growing range of Buy now, pay later (BNPL) payment solutions, Riverty offers you and your online shoppers an outstanding webshop experience. You get simple and fast integration plus the guarantee of being paid, while your customers get more freedom, safety and security.
                It’s win-win for you and your customers.
                <br>
                <p>
                    With our seamless integration you can quickly get started with the payment options you want to offer (depending on the countries your webshop operates in).
                </p>
            </p>
            
            <p>
                <li class="AFTERPAY_PAYMENT_METHOD_LIST"><Strong>14-day invoice</strong> - Available in every market, 14-day Invoice is the default payment method of Riverty. It is primary designed for business-to-consumer sales.</li>
                <li class="AFTERPAY_PAYMENT_METHOD_LIST"><Strong>Instalments</strong> - This is a Part Payment invoice. The customer can split their purchase over multiple monthly payments.</li>
                <li class="AFTERPAY_PAYMENT_METHOD_LIST"><Strong>Direct Debit</strong> - This is a Direct Debit version of the 14-day invoice payment method. The consumers IBAN is collected at time of purchase in the
                 check-out and the purchase amount is then debited from the consumer’s bank account once the goods have been delivered.</li>
                <li class="AFTERPAY_PAYMENT_METHOD_LIST"><Strong>Pay in 3</strong> - Particularly popular with younger, financially stable audiences buying premium value goods, Pay in 3 splits the check-out amount into 3 interest-free payment parts for completion within 90 days.</li>
            </p>
            
            <p>
                <strong>Riverty Payments are available in the following markets:</strong><br />
                <ul>
                    <li>Austria</li>
                    <li>Belgium</li>
                    <li>Denmark</li>
                    <li>Finland</li>
                    <li>Germany</li>
                    <li>Netherlands</li>
                    <li>Norway</li>
                    <li>Sweden</li>
                    <li>Switzerland</li>
                </ul>
            </p>
            <p>
                <strong>More information about the use and configuration of this module can be found on the <a href="https://developer.riverty.com/partner-and-integrations/plugins/prestashop/" target="_blank">Riverty Developer Portal.</a></strong>
            <p>
            <p>
                <strong>To retrieve an API key, <a href="https://www.riverty.com/en/business/support/contact-us/" target="_blank">contact Riverty</a> or create a sandbox API key on the <a href="https://developer-sandbox.riverty.com/" target="_blank">Riverty Developer Portal</a>.</strong>
        </div>
    </div>
</div>
