{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="#riverty" role="tab" data-toggle="tab">Riverty</a></li>
	<li><a href="#general" role="tab" data-toggle="tab">General Settings</a></li>
	<li><a href="#at" role="tab" data-toggle="tab">Austria</a></li>
	<li><a href="#be" role="tab" data-toggle="tab">Belgium</a></li>
	<li><a href="#dk" role="tab" data-toggle="tab">Denmark</a></li>
	<li><a href="#fi" role="tab" data-toggle="tab">Finland</a></li>
	<li><a href="#de" role="tab" data-toggle="tab">Germany</a></li>
	<li><a href="#nl" role="tab" data-toggle="tab">The Netherlands</a></li>
	<li><a href="#no" role="tab" data-toggle="tab">Norway</a></li>
	<li><a href="#se" role="tab" data-toggle="tab">Sweden</a></li>
	<li><a href="#ch" role="tab" data-toggle="tab">Switzerland</a></li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane active" id="riverty">{include file='./riverty.tpl'}</div>
	<div class="tab-pane" id="general">{include file='./general.tpl'}</div>
	<div class="tab-pane" id="at">{include file='./at.tpl'}</div>
	<div class="tab-pane" id="be">{include file='./be.tpl'}</div>
	<div class="tab-pane" id="dk">{include file='./dk.tpl'}</div>
	<div class="tab-pane" id="fi">{include file='./fi.tpl'}</div>
	<div class="tab-pane" id="de">{include file='./de.tpl'}</div>
	<div class="tab-pane" id="nl">{include file='./nl.tpl'}</div>
	<div class="tab-pane" id="no">{include file='./no.tpl'}</div>
	<div class="tab-pane" id="se">{include file='./se.tpl'}</div>
	<div class="tab-pane" id="ch">{include file='./ch.tpl'}</div>
</div>
