{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<form action="{$action|escape:'htmlall':'UTF-8'}" method="POST" id="payment-form-{$payment_option_id|escape:'htmlall':'UTF-8'}">
    <input type="hidden" name="payment_option_id" value="{$payment_option_id|escape:'htmlall':'UTF-8'}"/>
        <div class="sellingbox">
            <ul>
                <li><span>{l s='Pay in' mod='riverty'} {l s='%1$d'  sprintf=[$installments[0][5]|escape:'html':'UTF-8'] mod='riverty'} {l s='installments, starting at'  mod='riverty'} {l s='€' mod='riverty'} {$installments[0][7]} {l s=' per month'  mod='riverty'}.</span></li>
                <li><span>{l s='Manage all your payments in the Riverty app' mod='riverty'}.</span></li>
                <li><span>{l s='Receive your order before payment' mod='riverty'}.</span></li>
            </ul>
        </div>
    <div class="installments">
        {foreach from=$installments key=index item=installmentArray}
            <div class="installmentOption">
            {$encoded= json_encode($installmentArray)}
                <input type="radio" name="selectedInstallment" data-link="{$installmentArray[17]}" installmentsNo="{$installmentArray[5]}" debitInterest="{$installmentArray[9]}" effectiveInterest="{$installmentArray[10]}" class="radioButton" id="installmentOption{$index}" totalInterestRate="{$installmentArray[12]}" value="{$encoded}" required="required" >
                <label class="optionPrice" for="installmentOption{$index}" >{l s='gap' mod='riverty'} {l s='€' mod='riverty'} {$installmentArray[6]}  {l s='per month' mod='riverty'} <label class="installmentsNo"> {l s='in' mod='riverty'} {$installmentArray[5]} {l s='installments' mod='riverty'}</label> </label> 
            </div>
        {/foreach}
           <div class="form-check-label-mandatory installmentsText" name="installmentOption{$index}">*{l s='The shopping cart of'  mod='riverty'} {$installmentArray[4]}€ {l s='and'  mod='riverty'} <span id="installmentsNo"></span> {l s='installments'  mod='riverty'} {l s='results in a total loan amount of'  mod='riverty'} {$installmentArray[15]}€. {l s='Debit interest is'  mod='riverty'} <span id="debitInterest"></span>% {l s='and effective interest rate is'  mod='riverty'} <span id="effectiveInterest"></span>%.</div>
    </div>
    <div>
            <label class="credentials">{l s='Enter your credentials' mod='riverty'}</label>
            <p class="introText">{$text}</p>
        </div>
    {if $show_dob}
    <label>{l s='Date of birth' mod='riverty'}</label>
    <div class="form-group row">
        <div class="col-md-2">
            <input type="text" class="form-control" name="dob_year" id="dobYear"  required="required" maxlength="10">
        </div>
    </div>
    {/if}
    {if $show_phone}
    <div class="form-group">
        <label for="phonenumber">{l s='Phonenumber' mod='riverty'}</label>
        <input type="text" class="form-control" name="phonenumber" id="phonenumber" required="required">
    </div>
    {/if}
    {if $show_iban}
    <div class="form-group">
        <label for="bankiban">{l s='IBAN account' mod='riverty'}</label>
        <input type="text" class="form-control IBAN" id="IBAN1"  name="bankiban" required="required" onkeyup="sendPaymentMethodId('{$payment_option_id}')" />
            <span class="bank_validation_loader"> </span>
            <span class="check_mark"> </span>
            <span class="bank_validation_response"> </span>
    </div>
    {/if}
    {if $tracking == 'mandatory'}
        <div class="form-group" id='test'>
            <input class="form-check-input" type="checkbox" id="trackingBox9" name="trackingBox" style="margin-left:0!important" required data-session = "{session_id()}"  '>
            <label class='form-check-label'>{l s='I agree to the use of my data for the purpose of fraud prevention according to clause 4.1.2 of the' mod='riverty'}<a href="{'https://documents.myafterpay.com/privacy-statement/de_DE/default'|escape:'htmlall':'UTF-8'}" title="{l s=' privacy policy' mod='riverty'}" target="blank">{l s=' privacy policy' mod='riverty'}</a>{l s=' .' mod='riverty'} </label>
        </div>
    {/if}
    {if $tracking == 'optional'}
        <div class="form-group" id='test'>
            <input class="form-check-input" type="checkbox" id="trackingBox10" name="trackingBox" style="margin-left:0!important" required data-session = "{session_id()}" '>
            <label class='form-check-label'>{l s='I agree to the use of my data for the purpose of fraud prevention according to clause 4.1.2 of the' mod='riverty'}<a href="{'https://documents.myafterpay.com/privacy-statement/de_DE/default'|escape:'htmlall':'UTF-8'}" title="{l s=' privacy policy' mod='riverty'}" target="blank">{l s=' privacy policy' mod='riverty'}</a>{l s=' .' mod='riverty'} </label>
        </div>
    {/if}
    {if $customerConsent}
        <input class="form-check-input terms" type="checkbox" id="terms_{$payment_option_id|escape:'htmlall':'UTF-8'}" style="margin-left:0!important" required>
    {/if}
    <div class="form-group">
        <div class="form-check">
            <label class="form-check-label-mandatory" for="installmentInformation">
                {l s='Here you will find the mandatory information according to' mod='riverty'}
                    <span>
                        <a data-toggle="modal" data-target="#myModal11" href="{'https://documents.myafterpay.com/consumer-terms-conditions/de_DE/default/bgb507'|escape:'htmlall':'UTF-8'}" title="{l s='§ 507 BGB' mod='riverty'}" target="blank">{l s=' § 507 BGB' mod='riverty'}</a>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal11" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title ModalLongTitle">{l s='§ 507 BGB' mod='riverty'}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <object data="{'https://documents.myafterpay.com/consumer-terms-conditions/de_DE/default/bgb507'|escape:'htmlall':'UTF-8'}" type="text/html" class="objectData"> </object>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary closebtn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                {l s='and the' mod='riverty'}
                    <span>
                        <a data-toggle="modal" data-target="#myModal12" href="{''|escape:'htmlall':'UTF-8'}" title="{l s='the European Standard Information for Consumer Credit' mod='riverty'}" target="blank">{l s='the European Standard Information for Consumer Credit' mod='riverty'}</a>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal12" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title ModalLongTitle">{l s='European Standard Information for Consumer Credit' mod='riverty'}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <object data="{'https://documents.myafterpay.com/readmore/partpayment/fix/en_de/893/42.00_3_99_29.85_19.00_20.75_41.43_45.00_29.00_2999.46'|escape:'htmlall':'UTF-8'}" type="text/html" class="objectData"> </object>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary closebtn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                {l s='.' mod='riverty'}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="form-check">
            <label class="form-check-label-mandatory" for="terms">
                {l s='The' mod='riverty'}
                    <span>
                        <a data-toggle="modal" data-target="#myModal13" href="{$termsUrl|escape:'htmlall':'UTF-8'}" title="{l s='General Terms and Conditions' mod='riverty'}" target="blank">{l s='General Terms and Conditions' mod='riverty'}</a>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal13" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title ModalLongTitle">{l s='The Terms & Conditions for the Riverty payment method' mod='riverty'}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <object data="{$termsUrl|escape:'htmlall':'UTF-8'}" type="text/html" class="objectData"> </object>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary closebtn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                {l s='for the Riverty payment method apply. The Privacy Policy of Riverty can be found' mod='riverty'}
                    <span>
                        <a data-toggle="modal" data-target="#myModal14" href="{$privacyUrl|escape:'htmlall':'UTF-8'}" title="{l s='here' mod='riverty'}" target="blank">{l s='here' mod='riverty'}</a>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal14" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title ModalLongTitle">{l s='The privacy policy for the Riverty payment method' mod='riverty'}</h5>
                                    </div>
                                    <div class="modal-body">
                                        <object data="{$privacyUrl|escape:'htmlall':'UTF-8'}" type="text/html" class="objectData"> </object>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary closebtn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </span>
                {l s='.' mod='riverty'}
            </label>
        </div>
    </div>
</form>

{*
* Added transated text to the bank account validation
*}

<script type="text/javascript">
// <![CDATA[
    // Translations
    var bankValidationText = '{l s='Bank account validated' mod='riverty'}';
//]]>
    var tracking3 = '{$tracking}';
    var country = '{$payment_option_id}';
    var language = '{$language.iso_code}';
</script>
