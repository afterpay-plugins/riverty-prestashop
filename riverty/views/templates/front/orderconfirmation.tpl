{*
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="orderConfirmationContainer">
    <h4 class= "orderConfirmationTitle">{l s='Shop first, pay later with Riverty' mod='riverty'}</h4>
    <div> 
      <img class= "orderConfirmationImage" src= "https://cdn.myafterpay.com/logo/AfterPay_logo_checkout.svg">
    </div>
    <p class= "orderConfirmationText">{l s='Thank you for your purchase at Prestashop and for choosing Riverty as your payment method. An email with the order confirmation, details and tracking info is on the way, but feel free to discover what Riverty can do for you' mod='riverty'}.</p>
    <p class= "orderConfirmationText">{l s='Download our app on your smartphone or visit' mod='riverty'} <a class="orderConfirmationLink" href="https://my.riverty.com/" target="blank"> MyRiverty </a>{l s='in your browser to find out the easiest way to keep track of outstanding payments, pause payments for returns or keep track and many more options' mod='riverty'}.</p>
</div>
