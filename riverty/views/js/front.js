/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
$(document).ready(function () {
    $("input[data-module-name='afterpay_nl_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_nl_directdebit']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_nl_business']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_nl_payin3']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_be_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_de_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_de_directdebit']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_de_installments']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_at_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_at_directdebit']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_at_installments']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_ch_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_no_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_se_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_fi_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");
    $("input[data-module-name='afterpay_dk_digitalinvoice']").parent().parent().addClass("payment-option-afterpay");

    $("#myModal5").attr("id", "myModal9");
    $("#termsTarget").attr("data-target", "#myModal9");
    $("#myModal6").attr("id", "myModal10");
    $("#privacyTarget").attr("data-target", "#myModal10");
    setDateMask();

    // Display installemnts details and european inforamtion link upon selecting an installment plan
    var installmentsNo = $('#installmentOption0').attr('installmentsNo');
    var debitInterest = $('#installmentOption0').attr('debitInterest');
    var effectiveInterest = $('#installmentOption0').attr('effectiveInterest');
    $('#installmentsNo').html(installmentsNo);
    $('#debitInterest').html(debitInterest);
    $('#effectiveInterest').html(effectiveInterest);
    $('#effectiveInterest').html(effectiveInterest);
    $('#installmentOption0').parent().addClass("highlighted");

    $('input[type=radio][name="selectedInstallment"]').change(function () {
        var link = $(this).attr('data-link');
        var id = $(this).attr('id');
        $('a[data-target="#myModal12"]').attr('href', link);
        $('#europeanInformation').attr('data', link);

        var installmentsNo = $(this).attr('installmentsNo');
        var debitInterest = $(this).attr('debitInterest');
        var effectiveInterest = $(this).attr('effectiveInterest');
        $('#installmentsNo').html(installmentsNo);
        $('#debitInterest').html(debitInterest);
        $('#effectiveInterest').html(effectiveInterest);
        $("div").removeClass("highlighted");
        $(this).parent().addClass("highlighted");

        // Display total interest rate
        var totalInterestRate = $(this).attr('totalInterestRate');
        $('.totalInterestRate').remove();
        if (language != 'de') {
            $('.cart-summary-line.cart-total').append('<div class="interestContainer"> <span class="totalInterestRate">Entire interest of chosen installment </span> <span class="totalInterestRate">€ ' + totalInterestRate + '</span></div>');
        }
        if (language == 'de') {
            $('.cart-summary-line.cart-total').append('<div class="interestContainer"> <span class="totalInterestRate">Gesamtzinsen der gewählte Ratenzahlung </span> <span class="totalInterestRate">' + totalInterestRate + ' €</span></div>');
        }
    });

    $('input[type=radio][data-module-name="afterpay_de_installments"]').change(function (param) {
        // Automatically select first installment option
        $('#installmentOption0').prop("checked", true).trigger("change");
    });

    $('.installmentOption').click(function (e) {
        $(this).children('.radioButton').prop('checked', true).trigger("change");
    });

});

// Adding mask to dob and IBAN fields in checkout page
function setDateMask() {

    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js", function (data, textStatus, jqxhr) {

        $('input#dobYear').mask('00/00/0000', { placeholder: 'DD/MM/YYYY' });

        if (typeof language !== 'undefined') {
            if (language == 'nl') {
                $('input#dobYear').mask('00/00/0000', { placeholder: 'DD/MM/JJJJ' });
            }
        }
        if (typeof country !== 'undefined') {
            if (country.includes('at_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'AT00 0000 0000 0000 0000' });
            }
            if (country.includes('be_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'BE00 0000 0000 0000' });
            }
            if (country.includes('dk_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'DK00 0000 0000 0000 00' });
            }
            if (country.includes('fi_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'FI00 0000 0000 0000 00' });
            }
            if (country.includes('de_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'DE00 0000 0000 0000 0000 00' });
            }
            if (country.includes('nl_')) {
                $('input.IBAN').mask('SS00 SSSS 0000 0000 0000 00', { placeholder: 'NL00 XXXX 0000 0000 00' });
            }
            if (country.includes('no_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'NO00 0000 0000 000' });
            }
            if (country.includes('se_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'SE00 0000 0000 0000 0000 0000' });
            }
            if (country.includes('ch_')) {
                $('input.IBAN').mask('SS00 0000 0000 0000 0000 00', { placeholder: 'CH00 0000 0000 0000 0000 0' });
            }
        }
    });
};

// Adding Profile tracking script and noscript elements

const checkboxArray = [document.getElementById('trackingBox1'), document.getElementById('trackingBox2'), document.getElementById('trackingBox3'), document.getElementById('trackingBox4'), document.getElementById('trackingBox5'), document.getElementById('trackingBox6'), document.getElementById('trackingBox7'), document.getElementById('trackingBox8'), document.getElementById('trackingBox9'), document.getElementById('trackingBox10')];
var session = '';

checkboxArray.forEach((checkbox) => {
    if (checkbox) {
        session = $('#' + checkbox.id).data('session');
        loadProfileTrackingNoScript(checkbox);
    }
});

function loadProfileTrackingNoScript(checkbox) {
    checkbox.addEventListener('click', event => {
        if (event.target.checked) {
            var noscriptElement = document.createElement('noscript');
            noscriptElement.id = 'afterpay_tracking_noscript';
            document.getElementById('test').appendChild(noscriptElement);

            var imageElement = document.createElement('img');
            imageElement.src = '//uc8.tv/img/7982/' + session;
            imageElement.border = '0';
            imageElement.height = '0';
            imageElement.width = '0';

            noscriptElement.appendChild(imageElement);
            var scriptElementTag = document.getElementsByTagName('script')[0];
            scriptElementTag.parentNode.insertBefore(noscriptElement, scriptElementTag);

            var scriptElement = document.createElement('script');
            scriptElement.id = 'afterpay_tracking_script';
            scriptElement.type = 'text/javascript';
            scriptElement.async = true;
            scriptElement.src = '//uc8.tv/7982.js';

            var noscriptElementTag = document.getElementById('afterpay_tracking_noscript');
            noscriptElementTag.parentNode.insertBefore(scriptElement, noscriptElementTag);
        }
        if (!event.target.checked) {
            if (document.getElementById('afterpay_tracking_noscript')) {
                document.getElementById('afterpay_tracking_noscript').remove();
                document.getElementById('afterpay_tracking_script').remove();
            }
        }

    });
}

// Validate terms and conditions, and profile tracking checkboxesfor their respective countries
function validateTermsAndCond(paymentMethod, checkboxIdentifier) {
    $(paymentMethod).click(function (e) {
        $('[name="conditions_to_approve[terms-and-conditions]"]').click(function (e) {
            if ($('input:radio[name="payment-option"]:checked').attr('data-module-name') == "afterpay_" + checkboxIdentifier) {
                if ($(paymentMethod).prop('checked') == true) {
                    if($('#terms_' + checkboxIdentifier).is(':visible')){
                        if ($('#terms_' + checkboxIdentifier).is(':checked')) {
                        }
                        else {
                            e.preventDefault();
                        }
                    }
                }
            }
        });
    });
}


function validateProfileTracking(paymentMethod, tracking, checkboxIdentifier) {
    $(paymentMethod).click(function (e) {
        if (tracking == 'mandatory') {
            $('[name="conditions_to_approve[terms-and-conditions]"]').click(function (e) {
                if ($('input:radio[name="payment-option"]:checked').attr('data-module-name') == "afterpay_" + checkboxIdentifier) {
                    if ($('[name="trackingBox"]').length) {
                        if ($('[name="trackingBox"]').is(':checked')) {
                        }
                        else {
                            e.preventDefault();
                        }
                    }
                }
            });
        }
    });
}

function validateMandatoryTrackingCheck(paymentRadio, paymentMethod, tracking) {
    if (paymentRadio.attr('data-module-name') == paymentMethod) {
        if (typeof tracking !== 'undefined' && tracking == 'mandatory') {
            if ($('[name="trackingBox"]').length) {
                if (!$('[name="trackingBox"]').is(':checked')) {
                    alert('To proceed with payment using AfterPay, please accept the Data Privacy conditions.');
                }
            }
        }
    }
}

if ($('[name="conditions_to_approve[terms-and-conditions]"]').length) {
    let checkoutMainCheckBox = $('[name="conditions_to_approve[terms-and-conditions]"]');
    checkoutMainCheckBox.click(function (e) {
        let paymentRadio = $('input:radio[name="payment-option"]:checked');

        if (paymentRadio.attr('data-module-name') == "afterpay_be_digitalinvoice") {
            if (!$('#terms_be_digitalinvoice').is(':checked') && $('#terms_be_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_dk_digitalinvoice") {
            if (!$('#terms_dk_digitalinvoice').is(':checked') && $('#terms_dk_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_fi_digitalinvoice") {
            if (!$('#terms_fi_digitalinvoice').is(':checked') && $('#terms_fi_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_nl_digitalinvoice") {
            if (!$('#terms_nl_digitalinvoice').is(':checked') && $('#terms_nl_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_nl_directdebit") {
            if (!$('#terms_nl_directdebit').is(':checked') && $('#terms_nl_directdebit').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_no_digitalinvoice") {
            if (!$('#terms_no_digitalinvoice').is(':checked') && $('#terms_no_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_se_digitalinvoice") {
            if (!$('#terms_se_digitalinvoice').is(':checked') && $('#terms_se_digitalinvoice').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }
        if (paymentRadio.attr('data-module-name') == "afterpay_nl_payin3") {
            if (!$('#terms_nl_payin3').is(':checked') && $('#terms_nl_payin3').is(':visible')) {
                alert('To proceed with payment using AfterPay, please accept the terms and conditions');
            }
        }

        if (typeof country !== 'undefined') {

            if (country.includes('at_')) {
                if (typeof tracking1 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_at_digitalinvoice",tracking1);
                }
                if (typeof tracking2 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_at_directdebit",tracking2);
                }
                if (typeof tracking3 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_at_installments",tracking3);
                }
            }
        
            else if (country.includes('de_')) {
                if (typeof tracking1 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_de_digitalinvoice",tracking1);
                }
                if (typeof tracking2 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_de_directdebit",tracking2);
                }
                if (typeof tracking3 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_de_installments",tracking3);
                }
            }
        
            else if (country.includes('ch_')) {
                if (typeof tracking1 !== 'undefined') {
                    validateMandatoryTrackingCheck(paymentRadio,"afterpay_ch_digitalinvoice",tracking1);
                }
            }
        }

    });
}

if (typeof country !== 'undefined') {

    if (country.includes('at_')) {
        if (typeof tracking1 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_at_digitalinvoice"]', tracking1, 'at_digitalinvoice');
        }
        if (typeof tracking2 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_at_directdebit"]', tracking2, 'at_directdebit');
        }
        if (typeof tracking3 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_at_installments"]', tracking3, 'at_installments');
        }
    }

    else if (country.includes('de_')) {
        if (typeof tracking1 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_de_digitalinvoice"]', tracking1, 'de_digitalinvoice');
        }
        if (typeof tracking2 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_de_directdebit"]', tracking2, 'de_directdebit');
        }
        if (typeof tracking3 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_de_installments"]', tracking3, 'de_installments');
        }
    }

    else if (country.includes('ch_')) {
        if (typeof tracking1 !== 'undefined') {
            validateProfileTracking('[data-module-name="afterpay_ch_digitalinvoice"]', tracking1, 'ch_digitalinvoice');
        }
    }

    else {
        if (country.includes('be_')) {
            validateTermsAndCond('[data-module-name="afterpay_be_digitalinvoice"]', 'be_digitalinvoice');
        }
        if (country.includes('dk_')) {
            validateTermsAndCond('[data-module-name="afterpay_dk_digitalinvoice"]', 'dk_digitalinvoice');
        }
        if (country.includes('fi_')) {
            validateTermsAndCond('[data-module-name="afterpay_fi_digitalinvoice"]', 'fi_digitalinvoice');
        }
        if (country.includes('nl_')) {
            validateTermsAndCond('[data-module-name="afterpay_nl_digitalinvoice"]', 'nl_digitalinvoice');
            validateTermsAndCond('[data-module-name="afterpay_nl_directdebit"]', 'nl_directdebit');
            validateTermsAndCond('[data-module-name="afterpay_nl_payin3"]', 'nl_payin3');
        }
        if (country.includes('no_')) {
            validateTermsAndCond('[data-module-name="afterpay_no_digitalinvoice"]', 'no_digitalinvoice');
        }
        if (country.includes('se_')) {
            validateTermsAndCond('[data-module-name="afterpay_se_digitalinvoice"]', 'se_digitalinvoice');
        }
    }

}
// validating bank account using afterpay api request

var paymentMethodId;

function sendPaymentMethodId(paymentMethod) {
    paymentMethodId = paymentMethod;
}

jQuery(document).ready(function ($) {

    if (document.getElementById('IBAN') != null || document.getElementById('IBAN1') != null) {
        let input = document.getElementById('IBAN');
        let input1 = document.getElementById('IBAN1');
        let timeout = null;
        let preloader_timeout = null;

        if (input != null) {
            input.addEventListener('keyup', function (e) {
                var length = this.value.replace(/\s/g, '').length;
                if (length >= 16) {
                    clearTimeout(preloader_timeout);
                    clearTimeout(timeout);
                    preloader_timeout = setTimeout(preloader, 500);
                    timeout = setTimeout(sendRequest, 1000);
                }
            });
        }

        if (input1 != null) {
            input1.addEventListener('keyup', function (e) {
                var length = this.value.replace(/\s/g, '').length;
                if (length >= 16) {
                    clearTimeout(preloader_timeout);
                    clearTimeout(timeout);
                    preloader_timeout = setTimeout(preloader, 500);
                    timeout = setTimeout(sendRequest, 1000);
                }
            });
        }

        function preloader() {
            $(".bank_validation_response").css('display', 'none');
            $(".check_mark").css('display', 'none');
            $(".bank_validation_loader").addClass("spinner");
        }

        function sendRequest() {
            let bank_account_value = $('#IBAN').val();
            let bank_account_value1 = $('#IBAN1').val();
            let payment_Method_Id = paymentMethodId;

            $.ajax({
                url: ajax_link,
                type: 'POST',
                data: {
                    "IBAN": bank_account_value,
                    "IBAN1": bank_account_value1,
                    "paymentMethodId": payment_Method_Id,
                },
                dataType: "json",
                async: false,
            })
                .done(
                    function (response) {
                        $(".bank_validation_loader").removeClass("spinner");
                        $(".bank_validation_response").css('display', 'inline-block');
                        if (response.order_result.return.isValid) {
                            $(".check_mark").css('display', 'inline-block');
                            $(".bank_validation_response").removeClass("invalid_account");
                            $(".bank_validation_response").text(bankValidationText);
                        }
                        else {
                            $(".check_mark").css('display', 'none');
                            $(".bank_validation_response").addClass("invalid_account");
                            $(".bank_validation_response").text(response.order_result.return.riskCheckMessages[0].customerFacingMessage);
                        }
                    }
                ).fail(
                    function () {
                        $(".bank_validation_loader").removeClass("spinner");
                        $(".bank_validation_response").removeClass("invalid_account");
                        $(".bank_validation_response").text("something went wrong");
                    }
                );
        }
    }
});
