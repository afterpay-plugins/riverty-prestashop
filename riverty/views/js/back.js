$(document).ready(function () {

  // Add profile tracking tooltip to admin panel select option
  function appendTooltip(element) {
    const img = document.createElement('img');
    img.id = 'tooltip' + '_' + element.id;
    img.src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Blue_question_mark_icon.svg/1200px-Blue_question_mark_icon.svg.png';
    img.width = 20;
    img.title = "To enable Profile Tracking please request your Profile Tracking ID from AfterPay.";
    element.parentNode.appendChild(img);
  }

  // Showing and hiding profile tracking ID field depending on select option and added validation when enabled
  function hideProfileTrackingElement(element) {
    document.getElementById(element.id).parentNode.parentNode.style.display = 'none';
  }

  function showProfileTrackingElement(element) {
    document.getElementById(element.id).parentNode.parentNode.style.display = 'block';
  }

  //funciton to controll all show and hide profile tracking fields.
  function controlProfileTrackingElement(trackingElement, trackingElementId) {
    if (document.getElementById(trackingElement)) {
      element = document.getElementById(trackingElement);
      appendTooltip(element);
      elementValue = document.getElementById(trackingElement).value;
      if (elementValue == "Disabled") {
        if (document.getElementById(trackingElementId)) {
          hideProfileTrackingElement(document.getElementById(trackingElementId));
        }
      }
    }
  }

  controlProfileTrackingElement('AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING_ID');

  controlProfileTrackingElement('AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING_ID');


  //function for providing error message when tracking id is not entered 
  function showTrackingIdAlert(submitButton, trackingField, trackingIdField) {
    const button = $('#' + submitButton);
    if (button.attr('name') === 'submitAfterpayModule') {
      $(button).on('click', (event) => {
        if (!$('#' + trackingIdField).val()) {
          if ($('#' + trackingField).val() != "Disabled") {
            alert('Please make sure to enter Tracking ID in order to activate Profile Tracking Services');
            event.preventDefault();
          }
        }
      });
    }
  }

  showTrackingIdAlert('module_form_submit_btn_2', 'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_3', 'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_4', 'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_7', 'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_8', 'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_9', 'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING_ID');

  showTrackingIdAlert('module_form_submit_btn_6', 'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING_ID');

  //function to show or hide the traking id field 
  function controlTrackingField(trackingField, trackingFieldId) {
    $('#' + trackingField).on('change', function (e) {
      option = document.getElementById(trackingField).value;
      if (option == "Enabled-mandatory" || option == "Enabled-optional") {
        if (document.getElementById(trackingFieldId)) {
          showProfileTrackingElement(document.getElementById(trackingFieldId));
          document.getElementById(trackingFieldId).setAttribute('required', 'true');
        }
      }
      else {
        if (document.getElementById(trackingFieldId)) {
          hideProfileTrackingElement(document.getElementById(trackingFieldId));
        }
      }
    });
  }

  controlTrackingField('AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_AT_DIGITALINVOICE_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_AT_DIRECTDEBIT_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_AT_INSTALLMENTS_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_DE_DIGITALINVOICE_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING', 'AFTERPAY_DE_DIRECTDEBIT_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING', 'AFTERPAY_DE_INSTALLMENTS_PROFILE_TRACKING_ID');

  controlTrackingField('AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING', 'AFTERPAY_CH_DIGITALINVOICE_PROFILE_TRACKING_ID');

  // Add confirmation message to configuration settings forms

  function adminFormValidateMain(formNumber, dobRadioName, radioNameOff, fieldSet, confirmMessage) {
    let moduleForm = $('#' + formNumber);
    let flag = true;

    $("input[type=radio][name=" + dobRadioName + "]").change(function () {

      if ($("#" + radioNameOff).prop('checked') == true) {
        let message = 'DOB field is mandatory for this country. Please enable it !'

        $("#" + fieldSet).children('.form-wrapper').append
          ('<div class="alert alert-danger" role="alert" id="dobAlert">' + message + '</div>');
        flag = false;
      } else {
        $("#dobAlert").remove();
        flag = true;
      }
    });

    moduleForm.on('submit', function (event) {
      if (flag) {
        if (confirm(confirmMessage)) {
          return true;
        }
        else event.preventDefault();
      } else {
        event.preventDefault();
      }
    }
    );

  }

  function reqAdminFormConfirmation(formNumber, confirmationMessage) {
    let moduleForm = $('#' + formNumber);
    moduleForm.on('submit', function (event) {
      if (confirm(confirmationMessage)) {
        return true;
      }
      else event.preventDefault();
    }
    );
  }

  adminFormValidateMain('module_form_17', 'AFTERPAY_DK_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_DK_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_17', 'Are you sure you want to save 14-day Invoice?');

  adminFormValidateMain('module_form_2', 'AFTERPAY_AT_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_AT_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_2', 'Are you sure you want to save 14-day Invoice?');

  adminFormValidateMain('module_form_3', 'AFTERPAY_AT_DIRECTDEBIT_SHOW_DOB', 'AFTERPAY_AT_DIRECTDEBIT_SHOW_DOB_off',
    'fieldset_0_3', 'Are you sure you want to save Direct Debit?');

  adminFormValidateMain('module_form_4', 'AFTERPAY_AT_INSTALLMENTS_SHOW_DOB', 'AFTERPAY_AT_INSTALLMENTS_SHOW_DOB_off',
    'fieldset_0_4', 'Are you sure you want to save Installments?');

  adminFormValidateMain('module_form_5', 'AFTERPAY_BE_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_BE_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_5', 'Are you sure you want to save 14-day Invoice?');

  adminFormValidateMain('module_form_7', 'AFTERPAY_DE_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_DE_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_7', 'Are you sure you want to save 14-day Invoice?');

  adminFormValidateMain('module_form_8', 'AFTERPAY_DE_DIRECTDEBIT_SHOW_DOB', 'AFTERPAY_DE_DIRECTDEBIT_SHOW_DOB_off',
    'fieldset_0_8', 'Are you sure you want to save Direct Debit?');

  adminFormValidateMain('module_form_9', 'AFTERPAY_DE_INSTALLMENTS_SHOW_DOB', 'AFTERPAY_DE_INSTALLMENTS_SHOW_DOB_off',
    'fieldset_0_9', 'Are you sure you want to save Installments?');

  adminFormValidateMain('module_form_10', 'AFTERPAY_NL_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_NL_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_10', 'Are you sure you want to save 14-day Invoice?');

  adminFormValidateMain('module_form_11', 'AFTERPAY_NL_DIRECTDEBIT_SHOW_DOB', 'AFTERPAY_NL_DIRECTDEBIT_SHOW_DOB_off',
    'fieldset_0_11', 'Are you sure you want to save Direct Debit?');

  adminFormValidateMain('module_form_13', 'AFTERPAY_NL_PAYIN3_SHOW_DOB', 'AFTERPAY_NL_PAYIN3_SHOW_DOB_off',
    'fieldset_0_13', 'Are you sure you want to save Pay in 3?');

  adminFormValidateMain('module_form_6', 'AFTERPAY_CH_DIGITALINVOICE_SHOW_DOB', 'AFTERPAY_CH_DIGITALINVOICE_SHOW_DOB_off',
    'fieldset_0_6', 'Are you sure you want to save 14-day Invoice?');

  reqAdminFormConfirmation('module_form_16', 'Are you sure you want to save 14-day Invoice?');

  reqAdminFormConfirmation('module_form_15', 'Are you sure you want to save 14-day Invoice?');

  reqAdminFormConfirmation('module_form_14', 'Are you sure you want to save 14-day Invoice?');

  //order management form 
  reqAdminFormConfirmation('module_form_1', 'Are you sure you want to save Order management?');

  //main module settings 
  $("button[name='submitAfterpayModule']").click(function () {
    let moduleForm = $('#module_form');
    moduleForm.on('submit', function (event) {
      if (confirm('Are you sure you want to save Settings?')) {
        return true;
      }
      else event.preventDefault();
    }
    );
  });

  $('#module_form_12').on('submit', function (event) {
    if (confirm('Are you sure you want to save Business 2 Business?')) {
      return true;
    }
    else event.preventDefault();
  });

  $(".AFTERPAY_TEST_API_BUTTON").click(function (event) {
    let buttonName = $(this).attr("name"),
      testMode = buttonName.replace("TEST_API", "TESTMODE"),
      testModeValue = $("#" + testMode + " :selected").val();
    apiKey = buttonName.replace("TEST_API", testModeValue.toUpperCase() + "_API_KEY"),
      country = buttonName.substr(9, 2),
      apiKeyValue = $("#" + apiKey).val(),

      $.ajax({
        type: 'POST',
        url: validate_link,
        data: { "testAPI": true, "apiKeyValue": apiKeyValue, "testModeValue": testModeValue, "country": country },
        dataType: "json",
        async: false,
      }).done(
        function (response) {
          if (response) {
            $.growl.notice({ title: "Test Successful", message: "API key is valid." });
          } else {
            $.growl.error({ title: "Test Failed", message: "API key is invalid." });
          }
        }
      ).fail(
        function () {
          $.growl.error({ title: "Error", message: "Please try again." });
        }
      );
  });

});
