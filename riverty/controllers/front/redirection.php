<?php

class RivertyRedirectionModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function initContent()
    {
        /**
         * @todo: Remove the return when the SCA confirms
         * [DP-1040]
         */

        return;

        parent::initContent();
        
        if (method_exists('Tools', 'getAdminToken')) {
            $secureTokenSca = Tools::getAdminTokenLite('riverty');
        } else {
            $secureTokenSca = 1;
        }

        if (Tools::getValue('token') == $secureTokenSca) {
            //get the info using the table
            $rowId = Tools::getValue('id');
            $redirectInfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT sca_info FROM ' . _DB_PREFIX_ . 'riverty_sca_info WHERE sca_record_id = ' . (int)$rowId);
            $redirectInfo = unserialize($redirectInfo);
       
            //get the order id
            $orderId = $redirectInfo['orderId'];
            $order = new Order($orderId);
            $paymentMethod = $order->payment;
            $orderNumber = $redirectInfo['orderNumber'];
            $cartId = $redirectInfo['cartId'];
        
            //get the API key and the connection mode
            $paymentId = $redirectInfo['idPayment'];
            $afterpayOrder = $redirectInfo['afterpayOrder'];
            $afterpayConnectionMode = $redirectInfo['afterpayConnectionMode'];
            $afterpayApiKey = $redirectInfo['afterpayApiKey'];

            //get the order status using the get order request
            $orderResult = $this->module->getSCAOderDetails($paymentMethod, $orderNumber, $afterpayConnectionMode, $afterpayApiKey);
            $orderResultStatus = $orderResult->return->orderDetails->status;
       
            //compare the order status and continue realted functions
            if ($orderResultStatus == 'Accepted') {
                $paymentStatus = $this->module->getOrderStateId('Riverty - Authorized');
                $order->setCurrentState($paymentStatus);
                $secureKey = Context::getContext()->customer->secure_key;

                //Check if the order should be captured.
                $useCaptures = Configuration::get('AFTERPAY_USE_CAPTURES');
                if ($useCaptures == 'yes-autocapture') {
                    $invoiceNumber = $order->invoice_number;
                    $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
                    $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
                    $captureResult = $this->module->redirectCaptureOrder(
                        $redirectInfo['orderId'],
                        $invoiceNumberFormatted,
                        $afterpayConnectionMode,
                        $afterpayApiKey,
                        $redirectInfo['afterpayOrderlines'],
                        $afterpayOrder,
                        $orderNumber
                    );
                    if ($captureResult === true) {
                        $paymentStatus = $this->module->getOrderStateId('Riverty - Captured');
                        $order->setCurrentState($paymentStatus);
                    } else {
                        $paymentStatus = $this->module->getOrderStateId('Payment error');
                        $order->setCurrentState($paymentStatus);
                    }
                }
                /**
                 * We need to delete the temporarily made row
                 * to decarease the perfomrance overheads and
                 * to avoid storage issues
                 */
                Db::getInstance()->delete('riverty_sca_info', 'sca_record_id = '.(int)$rowId);

                //send the customer to the order success page
                $this->redirectWithNotifications('index.php?controller=order-confirmation&id_cart='.$cartId.
                 '&id_module='.(int)$this->module->id.'&id_order='.$orderId.'&key='.$secureKey);
            } else {
                /**
                 * We need to delete the temporarily made row
                 * to decarease the perfomrance overheads and
                 * to avoid storage issues
                 */
                Db::getInstance()->delete('riverty_sca_info', 'sca_record_id = '.(int)$rowId);

                //send the customer to the home page and show the error message
                $checkoutLink = $this->context->link->getPageLink(
                    'order',
                    null,
                    $this->context->language->id,
                    [
                        'action' => 'show',
                    ]
                );

                $paymentStatus = Configuration::get('PS_OS_ERROR');
                $order->setCurrentState($paymentStatus);

                $this->warning[] = $this->module->l('Strong authentication failed or was aborted. Please try again or select another payment method.');
                $this->redirectWithNotifications($checkoutLink);
            }
        } else {
            $rowId = Tools::getValue('id');
            $redirectInfo = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT sca_info FROM ' . _DB_PREFIX_ . 'riverty_sca_info WHERE sca_record_id = ' . (int)$rowId);
            $redirectInfo = unserialize($redirectInfo);

            $checkoutLink = $this->context->link->getPageLink(
                'order',
                null,
                $this->context->language->id,
                [
                    'action' => 'show',
                ]
            );
       
            //get the order id
            $orderId = $redirectInfo['orderId'];
            $order = new Order($orderId);
            $paymentStatus = Configuration::get('PS_OS_ERROR');
            $order->setCurrentState($paymentStatus);
            $this->warning[] = $this->module->l('A security issue occurred while processing payment');
            $this->redirectWithNotifications($checkoutLink);
        }
    }
}
