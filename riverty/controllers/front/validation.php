<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License version 3.0
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License version 3.0
 */

class RivertyValidationModuleFrontController extends ModuleFrontController
{
    private $afterpay_order;
    private $afterpayOrderlines;
    private $paymentMethodId;

    /**
     * This class should be use by your Instant Payment
     * Notification system to validate the order remotely
     */
    public function postProcess()
    {
        /*
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die;
        }

        /**
         * Get the cart information.
         */
        $cart = $this->context->cart;
        $cartId = (int) $cart->id;
        $amount = (float)$cart->getOrderTotal(true, Cart::BOTH);

        // Save old card features for restoring cart if payment failed.
        $oldCartSecureKey = $this->context->cart->secure_key;
        $oldCartCustomerId = (int)$this->context->cart->id_customer;
        $oldCartProducts = $this->context->cart->getProducts();

        $this->module->writeLog('Start postProcess of Validation');

        $secureKey = Context::getContext()->customer->secure_key;
        $moduleName = $this->module->displayName;
        $currencyId = (int) Context::getContext()->currency->id;

        $checkoutLink = $this->context->link->getPageLink(
            'order',
            null,
            $this->context->language->id,
            [
                'action' => 'show',
            ]
        );

        if ($this->isValidOrder() === true) {
            $paymentStatus = $this->module->getOrderStateId('Riverty - Awaiting payment');

            // Order is valid and should be created, because AfterPay needs an order number.
            $this->module->validateOrder(
                $cartId,
                $paymentStatus,
                $amount,
                $moduleName,
                null,
                array(),
                $currencyId,
                false,
                $secureKey
            );

            // Get the order history to set new order status.
            $history = new OrderHistory();
            $history->id_order = (int) $this->module->currentOrder;

            // Place order at AfterPay, if not succesfull update order to
            $transactionId = $this->placeOrder($this->module->currentOrder);
            if ($transactionId !== false) {
                // First set order to default remote payment accepted state.
                $paymentStatus = Configuration::get('PS_OS_WS_PAYMENT');
                $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                $history->add();

                //check whether we need to send the customer to the SCA page
                if ($this->context->cookie->riverty_pending_session) {
                    
                    // Get authorization key.
                    $afterpayApiKey = Configuration::get(
                        'AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_API_KEY'
                    );

                    // Get connection mode.
                    $afterpayConnectionMode = $this->module->getConnectionMode($this->paymentMethodId);

                    //create a sessoin with the neccessary data
                    $redirectInfo = [];
                    $redirectInfo['transactionId'] = $transactionId;
                    $redirectInfo['orderId'] = (int) $this->module->currentOrder;
                    $redirectInfo['afterpayOrderlines'] = $this->afterpayOrderlines;
                    $redirectInfo['orderNumber'] = $this->module->currentOrderReference;
                    $redirectInfo['cartId'] = (int)$cart->id;
                    $redirectInfo['idPayment'] = $this->paymentMethodId;
                    $redirectInfo['afterpayOrder'] = $this->afterpay_order;

                    /**
                     * In case there can be APIkey changes, so it is better to store 
                     * the API key and the endpoint
                     * This will be helpful if the order needs to be handled
                     * by the cron task
                     */
                    $redirectInfo['afterpayApiKey'] = $afterpayApiKey;
                    $redirectInfo['afterpayConnectionMode'] = $afterpayConnectionMode;

                    Db::getInstance()->insert('riverty_sca_info',[ 
                        'sca_info' => serialize($redirectInfo)
                    ]);
                    $rowId = Db::getInstance()->Insert_ID();

                    if($rowId == 0){
                         // Change order status.
                        $this->module->writeLog('Problem with inserting the order info to the DB');
                        $paymentStatus = Configuration::get('PS_OS_ERROR');
                        $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                        $history->add();
                        $this->warning[] = $this->module->l('An error occurred while processing payment');
                        $this->redirectWithNotifications($checkoutLink);
                    }

                    $secureUrl = $this->context->cookie->riverty_secure_url;

                    unset($this->context->cookie->riverty_secure_url);

                    if (method_exists('Tools', 'getAdminToken')) {
                        $secureTokenSca = Tools::getAdminTokenLite('riverty');
                    } else {
                        $secureTokenSca = 1;
                    }
                    
                    $redirectUrl = $secureUrl.urlencode(Context::getContext()->link->getModuleLink('riverty', 'redirection', array('token' => $secureTokenSca, 'id' => $rowId)));

                    // Save the AfterPay payment method and transaction ID.
                    $orderPaymentArray = OrderPayment::getByOrderReference($this->module->currentOrderReference);
                    if (is_array($orderPaymentArray)) {
                        $orderPayment = current($orderPaymentArray);
                        $orderPayment->payment_method = 'afterpay_' . $this->paymentMethodId;
                        $orderPayment->transaction_id = $transactionId;
                        $orderPayment->save();
                    }
                    
                    Tools::redirect($redirectUrl);
                }

                // Then set order to AfterPay payment accepted state.
                $paymentStatus = $this->module->getOrderStateId('Riverty - Authorized');
                $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                $history->add();

                // Save the AfterPay payment method and transaction ID.
                $orderPaymentArray = OrderPayment::getByOrderReference($this->module->currentOrderReference);
                if (is_array($orderPaymentArray)) {
                    $orderPayment = current($orderPaymentArray);
                    $orderPayment->payment_method = 'afterpay_' . $this->paymentMethodId;
                    $orderPayment->transaction_id = $transactionId;
                    $orderPayment->save();
                }

                // Check if the order should be captured.
                $useCaptures = Configuration::get('AFTERPAY_USE_CAPTURES');
                if ($useCaptures == 'yes-autocapture') {
                    $order = new Order($this->module->currentOrder);
                    $invoiceNumber = $order->invoice_number;
                    $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
                    $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
                    $captureResult = $this->captureOrder($this->module->currentOrder, $invoiceNumberFormatted);
                    if ($captureResult === true) {
                        $paymentStatus = $this->module->getOrderStateId('Riverty - Captured');
                        $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                        $history->add();
                    } else {
                        $paymentStatus = $this->module->getOrderStateId('Payment error');
                        $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                        $history->add();
                    }
                }

                $this->redirectWithNotifications('index.php?controller=order-confirmation&id_cart='.(int)$cart->id.
                '&id_module='.(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$secureKey);
            } else {
                // Change order status.
                $this->module->writeLog('Problem with validating order: order not valid');
                $paymentStatus = Configuration::get('PS_OS_ERROR');
                $history->changeIdOrderState($paymentStatus, (int)($this->module->currentOrder));
                $history->add();

                if (isset($this->warning[0])) {
                    $checkoutLink = $this->context->link->getPageLink(
                        'order',
                        null,
                        $this->context->language->id,
                        [
                            'action' => 'show',
                            'message' => $this->warning[0]
                        ]
                    );
                }

                // Restore old cart in new cart.
                $this->restoreOldCart($oldCartProducts, $oldCartCustomerId, $oldCartSecureKey);

                // Redirect with notifications.
                $this->redirectWithNotifications($checkoutLink);
            }
        } else {
            /**
             * Add a message to explain why the order has not been validated
             */
            $this->warning[] = $this->module->l('An error occurred while processing payment');
            $this->redirectWithNotifications($checkoutLink);
        }
    }

    protected function isValidOrder()
    {
        // Get the payment information.
        if (isset($_REQUEST['payment_option_id'])) {
            $this->paymentMethodId = $_REQUEST['payment_option_id'];
        } else {
            $this->module->writeLog('Problem with validating order: no payment option set');
            return false;
        }

        // Prepare data for AfterPay validation.
        $cart = Context::getContext()->cart;
        $currency = Context::getContext()->currency->iso_code;
        $addressInvoice = new Address($cart->id_address_invoice);
        $addressDelivery = new Address($cart->id_address_delivery);
        $addressInvoiceSplit = $this->splitAfterpayAddress(
            trim($addressInvoice->address1 . ' ' . $addressInvoice->address2)
        );
        $addressInvoiceCountry = new Country($addressInvoice->id_country);
        $addressInvoiceCountry = $addressInvoiceCountry->iso_code;
        $addressDeliverySplit = $this->splitAfterpayAddress(
            trim($addressDelivery->address1 . ' ' . $addressDelivery->address2)
        );
        $addressDeliveryCountry = new Country($addressDelivery->id_country);
        $addressDeliveryCountry = $addressDeliveryCountry->iso_code;
        $customer = Context::getContext()->customer;
        $language = new Language($customer->id_lang);
        $language = Tools::strtoupper($language->iso_code);

        // Check which field should be used.
        $useDob = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_DOB');
        $useIban = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_IBAN');
        $useCompany = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_COMPANY');
        $useCoc = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_COC');
        $useSSN = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_SSN');
        $useTracking = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_PROFILE_TRACKING');
        $usePhone = Configuration::get('AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_SHOW_PHONE');

        // Get session ID for profile Tracking
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        @session_start();
        $profileTrackingId = substr(substr(md5(session_id()), -6) . '-' . mt_rand(1000, 9999), 0, 11);

        //Get phone
        if ($usePhone == true) {
            $checkoutLink = $this->context->link->getPageLink(
                'order',
                null,
                $this->context->language->id,
                [
                    'action' => 'show',
                ]
            );

            if (!$_REQUEST['phonenumber']) {
                $this-> warning[] = $this->l(
                    'Please make sure the phone number is correct.',
                    'validation'
                );
                $this->redirectWithNotifications($checkoutLink);
            }
        }

        // Get birthdate.
        if ($useDob == true) {
            $dobYear = (isset($_REQUEST['dob_year'])) ? $_REQUEST['dob_year'] : null ;

            if (isset($_REQUEST['dob_year'])) {
                foreach (explode('/', $_REQUEST['dob_year']) as $key => $value) {
                    ${'var'.$key} = $value;
                }
            }

            $checkoutLink = $this->context->link->getPageLink(
                'order',
                null,
                $this->context->language->id,
                [
                    'action' => 'show',
                ]
            );

            // Check if the entered DOB is correct and the person is 18+
            if ($var0 <= 0) {
                $this-> warning[] = $this->l(
                    'Please make sure the date of birth is correct.'
                );
                $this->redirectWithNotifications($checkoutLink);
            } elseif ($var0 > 31) {
                $this-> warning[] = $this->l(
                    'Please make sure the date of birth is correct.'
                );
                $this->redirectWithNotifications($checkoutLink);
            } elseif ($var1 <= 0) {
                $this-> warning[] = $this->l(
                    'Please make sure the date of birth is correct.'
                );
                $this->redirectWithNotifications($checkoutLink);
            } elseif ($var1 > 12) {
                $this-> warning[] = $this->l(
                    'Please make sure the date of birth is correct.'
                );
                $this->redirectWithNotifications($checkoutLink);
            } else {
                $date2= date("d-m-Y"); //today's date
                $date1= new DateTime($var0.'-'.$var1.'-'.$var2);
                $date2= new DateTime($date2);
                
                if ($date1 < $date2) {
                    $interval = date_diff($date1, $date2);
                    $age = $interval->y;
                    if ($age >= 18) {
                        $dob = $var2.'-'.$var1.'-'.$var0 . 'T00:00:00';
                    } else {
                        $this->warning[] = $this->module->l('To make use of the Riverty payment method, your age has to be 18 years or older.', 'validation');
                        $this->redirectWithNotifications($checkoutLink);
                    }
                } else {
                    $this->warning[] = $this->l(
                        'Please make sure the date of birth is correct.'
                    );
                    $this->redirectWithNotifications($checkoutLink);
                }
            }
        }

        // Get bank iban.
        if ($useIban == true) {
            $bankIban = (isset($_REQUEST['bankiban'])) ? $_REQUEST['bankiban'] : null ;
        }

        // Get company details.
        if ($useCompany == true) {
            $companyName = (isset($_REQUEST['company'])) ? $_REQUEST['company'] : '' ;
        }
        if ($useCoc == true) {
            $cocNumber = (isset($_REQUEST['coc'])) ? $_REQUEST['coc'] : '' ;
        }

        // Get SSN number
        if ($useSSN == true) {
            $ssnNumber = (isset($_REQUEST['ssn'])) ? $_REQUEST['ssn'] : '' ;
        }

        // Get phonenumber.
        $phonenumberInvoice = $addressInvoice->phone;
        $phonenumberInvoiceForm = (isset($_REQUEST['phonenumber'])) ? $_REQUEST['phonenumber'] : $phonenumberInvoice ;
        $phonenumberDelivery = $addressDelivery->phone;
        $phonenumberDeliveryForm = (isset($_REQUEST['phonenumber'])) ? $_REQUEST['phonenumber'] : $phonenumberDelivery ;

        // Get installments information
        $installmentsData = json_decode($_REQUEST['selectedInstallment']);

        $profileNo = (isset($installmentsData[16])) ? $installmentsData[16] : null ;
        $numberOfInstallments = (isset($installmentsData[5])) ? $installmentsData[5] : null ;
        $customerInterestRate = (isset($installmentsData[9])) ? $installmentsData[9] : null ;


        // Get client IP address.
        $clientIpaddress = Tools::getRemoteAddr();

        // Build up the order in format for AfterPay.
        $afterpayOrder = [];
        $afterpayOrder['billtoaddress']['city'] = $addressInvoice->city;
        $afterpayOrder['billtoaddress']['housenumber'] = '';
        if ($addressInvoiceSplit[1] != '') {
            $afterpayOrder['billtoaddress']['housenumber'] = $addressInvoiceSplit[1];
        }
        if ($addressInvoiceSplit[2] != '') {
            $afterpayOrder['billtoaddress']['housenumberaddition'] = $addressInvoiceSplit[2];
        }
        $afterpayOrder['billtoaddress']['isocountrycode'] = $addressInvoiceCountry;
        $afterpayOrder['billtoaddress']['postalcode'] = $addressInvoice->postcode;
        if ($useDob == true) {
            $afterpayOrder['billtoaddress']['referenceperson']['dob'] = $dob;
        }
        if ($useSSN == true) {
            $afterpayOrder['billtoaddress']['referenceperson']['ssn'] = $ssnNumber;
        }
        $afterpayOrder['billtoaddress']['referenceperson']['email'] = $customer->email;
        $afterpayOrder['billtoaddress']['referenceperson']['isolanguage'] = $language;
        $afterpayOrder['billtoaddress']['referenceperson']['firstname'] = $addressInvoice->firstname;
        $afterpayOrder['billtoaddress']['referenceperson']['lastname'] = $addressInvoice->lastname;
        $afterpayOrder['billtoaddress']['referenceperson']['phonenumber'] = $phonenumberInvoiceForm;
        $afterpayOrder['billtoaddress']['streetname'] = $addressInvoiceSplit[0];

        // Set up the ship to address.
        $afterpayOrder['shiptoaddress']['city'] = $addressDelivery->city;
        $afterpayOrder['shiptoaddress']['housenumber'] = '';
        if ($addressDeliverySplit[1] != '') {
            $afterpayOrder['shiptoaddress']['housenumber'] = $addressDeliverySplit[1];
        }
        if ($addressDeliverySplit[2] != '') {
            $afterpayOrder['shiptoaddress']['housenumberaddition'] = $addressDeliverySplit[2];
        }
        $afterpayOrder['shiptoaddress']['isocountrycode'] = $addressDeliveryCountry;
        $afterpayOrder['shiptoaddress']['postalcode'] = $addressDelivery->postcode;
        $afterpayOrder['shiptoaddress']['referenceperson']['isolanguage'] = $language;
        $afterpayOrder['shiptoaddress']['referenceperson']['firstname'] = $addressDelivery->firstname;
        $afterpayOrder['shiptoaddress']['referenceperson']['lastname'] = $addressDelivery->lastname;
        $afterpayOrder['shiptoaddress']['referenceperson']['phonenumber'] = $phonenumberDeliveryForm;
        $afterpayOrder['shiptoaddress']['streetname'] = $addressDeliverySplit[0];

        // Set up the additional information.
        $afterpayOrder['currency'] = $currency;
        $afterpayOrder['ipaddress'] = $clientIpaddress;
        if ($useTracking == "Enabled-optional" && isset($_REQUEST['trackingBox'])) {
            $afterpayOrder['profileTrackingId'] = $profileTrackingId;
        } elseif ($useTracking == "Enabled-mandatory") {
            $afterpayOrder['profileTrackingId'] = $profileTrackingId;
        }
        $afterpayOrder['additionalData']['pluginProvider'] = $this->module->author;
        $afterpayOrder['additionalData']['pluginVersion'] = $this->module->version;
        $afterpayOrder['additionalData']['shopUrl'] = Tools::getHttpHost(true).Configuration::get('PS_BASE_URL');
        $afterpayOrder['additionalData']['shopPlatform'] = 'Prestashop';
        $afterpayOrder['additionalData']['shopPlatformVersion'] = Configuration::get('PS_INSTALL_VERSION');

        // Set B2B values.
        if (in_array($this->paymentMethodId, ['nl_business'])) {
            $afterpayOrder['company']['companyname'] = $companyName;
            if ($useCoc == true) {
                $afterpayOrder['company']['identificationNumber'] = $cocNumber;
            }
        }

        // Set bank details
        if ($useIban == true && strpos($this->paymentMethodId, 'directdebit')) {
            $afterpayOrder['directDebit']['bankAccount'] = $bankIban;
            if (empty($bankIban)) {
                $this->warning[] = $this->module->l('Please enter your IBAN number.', 'validation');
                return false;
            }
        }

        //set pay in 3 due amount
        if (strpos($this->paymentMethodId, 'payin3')) {
            $afterpayOrder['payInX']['dueAmount'] = $_REQUEST['payin3_due_amount'];
        }

        // Set installment details
        if (strpos($this->paymentMethodId, 'installments')) {
            $afterpayOrder['installment']['bankAccount'] = $bankIban;
            $afterpayOrder['installment']['profileNo'] = $profileNo;
            $afterpayOrder['installment']['numberOfInstallments'] = $numberOfInstallments;
            $afterpayOrder['installment']['customerInterestRate'] = $customerInterestRate;
            if (empty($numberOfInstallments)) {
                $this->warning[] = $this->l(
                    'Please choose an installment plan.'
                );
                $this->redirectWithNotifications($checkoutLink);
            }
        }

        $cartOrderlines = $cart->getProducts(true);
        if (count($cartOrderlines) == 0) {
            return 'Problem with orderlines';
        }

        // Set up order lines, repeat for more order lines.
        $afterpayOrderlines = [];
        foreach ($cartOrderlines as $cartOrderline) {
            // Get Product URL.
            $productUrl = Context::getContext()->link->getProductLink($cartOrderline['id_product']);
            $afterpayOrderline = [];
            $afterpayOrderline['productUrl'] = $productUrl;

            // Get Image URL;
            $imageUrl = Context::getContext()->link->getImageLink(
                $cartOrderline['link_rewrite'],
                $cartOrderline['id_image']
            );
            $afterpayOrderline['imageUrl'] = $imageUrl;

            $afterpayOrderline['sku'] = ($cartOrderline['reference'] != '') ? $cartOrderline['reference'] : 'PRODUCTID';
            $afterpayOrderline['name'] = $cartOrderline['name'];
            $afterpayOrderline['qty'] = $cartOrderline['cart_quantity'];
            $priceIncl = (string) (round($cartOrderline['price_with_reduction'], 2) * 100);
            $vatAmount = (string) (
                $cartOrderline['price_with_reduction'] - round($cartOrderline['price_with_reduction_without_tax'], 2)
            );
            $afterpayOrderline['price'] = $priceIncl;
            $afterpayOrderline['vat_amount'] = round($vatAmount, 2);
            $afterpayOrderlines[] = $afterpayOrderline;
        }

        // Add shipping cost.
        $shippingAmountIncl = $cart->getPackageShippingCost(null, true);
        $shippingAmountExcl = $cart->getPackageShippingCost(null, false);
        $shippingAmountVat = $shippingAmountIncl - $shippingAmountExcl;

        if ($shippingAmountIncl > 0) {
            $afterpayShippingLine = [];
            $afterpayShippingLine['sku'] = $this->l('SHIPPINGCOST');
            $afterpayShippingLine['name'] = $this->l('Shipping cost');
            $afterpayShippingLine['qty'] = 1;
            $afterpayShippingLine['price_excl'] = $shippingAmountExcl * 100;
            $afterpayShippingLine['price'] = $shippingAmountIncl * 100;
            $afterpayShippingLine['vat_amount'] = round($shippingAmountVat, 2);
            $afterpayOrderlines[] = $afterpayShippingLine;
        }

        // Check if discounts are used.
        $discountAmountIncl = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
        $discountAmountExcl = $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS);
        $discountAmountVat = $discountAmountIncl - $discountAmountExcl;

        if ($discountAmountIncl > 0) {
            $afterpayDiscountLine = [];
            $afterpayDiscountLine['sku'] = $this->l('DISCOUNT');
            $afterpayDiscountLine['name'] = $this->l('Discount');
            $afterpayDiscountLine['qty'] = 1;
            $afterpayDiscountLine['price'] = ($discountAmountIncl * 100) * -1;
            $afterpayDiscountLine['price_excl'] = ($discountAmountExcl * 100) * -1;
            $afterpayDiscountLine['vat_amount'] = round($discountAmountVat, 2) * -1;
            $afterpayOrderlines[] = $afterpayDiscountLine;
        }

        $this->afterpay_order = $afterpayOrder;
        $this->afterpayOrderlines = $afterpayOrderlines;

        return true;
    }

    protected function captureOrder($orderId, $invoiceId)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);
        $afterpay->set_ordermanagement('capture_partial');

        // Get connection mode.
        $afterpayConnectionMode = $this->module->getConnectionMode($this->paymentMethodId);

        // Get authorization key.
        $authorisation = [];
        $authorisation['apiKey'] = Configuration::get(
            'AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_'. Tools::strtoupper($afterpayConnectionMode) .'_API_KEY'
        );

        // Get order lines
        if (count($this->afterpayOrderlines) > 0) {
            foreach ($this->afterpayOrderlines as $orderline) {
                $afterpay->create_order_line(
                    $orderline['sku'],
                    $orderline['name'],
                    $orderline['qty'],
                    $orderline['price'],
                    null,
                    $orderline['vat_amount'],
                    null,
                    null,
                    $orderline['productUrl'],
                    $orderline['imageUrl'],
                    null
                );
            }
        }

        $order = new Order($orderId);

        $afterpayOrder = $this->afterpay_order;
        $afterpayOrder['ordernumber'] = $order->reference;
        $afterpayOrder['invoicenumber'] = $invoiceId;

        $this->module->writeLog('Start capture action for order: ' . $afterpayOrder['ordernumber']);

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order($afterpayOrder, 'OM');
            $this->module->writeLog('Capture request: ' . json_encode($afterpay, JSON_PRETTY_PRINT));
            $afterpay->do_request($authorisation, $afterpayConnectionMode);
            $this->module->writeLog('Capture result: ' . json_encode($afterpay->order_result, JSON_PRETTY_PRINT));
            $this->module->sendAfterpayDebugMail($afterpay);

            // Retreive response.
            if (isset($afterpay->order_result->return->resultId)) {
                switch ($afterpay->order_result->return->resultId) {
                    case 0:
                        // Capture was succesfull.
                        $this->module->writeLog('Capture was succesfull');
                        return true;
                    case 1:
                        // Capture failed.
                        $this->module->writeLog('Capture failed');
                        return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->module->writeLog('Exception during capturing order: '. $e->getMessage());
            return false;
        }
    }

    protected function placeOrder($orderId)
    {
        $afterpay = new Afterpay\Afterpay();
        $afterpay->setRest(true);

        // Get connection mode.
        $afterpayConnectionMode = $this->module->getConnectionMode($this->paymentMethodId);

        // Get authorization key.
        $authorisation = [];
        $authorisation['apiKey'] = Configuration::get(
            'AFTERPAY_' . Tools::strtoupper($this->paymentMethodId) . '_'. Tools::strtoupper($afterpayConnectionMode) .'_API_KEY'
        );

        // Get order type and set B2B values.
        $orderType = 'B2C';
        if (in_array($this->paymentMethodId, ['nl_business'])) {
            $orderType = 'B2B';
        }

        // Get Country.
        $country = $this->afterpay_order['billtoaddress']['isocountrycode'];

        // Get order lines.
        if (count($this->afterpayOrderlines) > 0) {
            foreach ($this->afterpayOrderlines as $orderline) {
                // If country is NL or BE, set VatCategory, otherwise is null.
                if (in_array($country, ['NL','BE'])) {
                    $vatCategory = $this->module->getAfterpayTaxClass(
                        $orderline['price'],
                        $orderline['vat_amount'] * 100
                    );
                } else {
                    $vatCategory = null;
                }

                $afterpay->create_order_line(
                    $orderline['sku'],
                    $orderline['name'],
                    $orderline['qty'],
                    $orderline['price'],
                    $vatCategory,
                    $orderline['vat_amount'],
                    null, // @todo: add $googleProductCategoryId
                    null, // @todo: add $googleProductCategory
                    (isset($orderline['productUrl'])) ? $orderline['productUrl'] : '',
                    (isset($orderline['imageUrl'])) ? $orderline['imageUrl'] : '',
                    null // @todo: add $groupId
                );
            }
        }

        $order = new Order($orderId);

        $afterpayOrder = $this->afterpay_order;
        $afterpayOrder['ordernumber'] = $order->reference;

        try {
            // Transmit all the specified data, from the steps above, to afterpay.
            $afterpay->set_order($afterpayOrder, $orderType);
            $this->module->writeLog('AfterPay Order: ' . json_encode($afterpayOrder, JSON_PRETTY_PRINT));
            $afterpay->do_request($authorisation, $afterpayConnectionMode);
            $this->module->writeLog($afterpay->client->getDebugLog());
            $this->module->sendAfterpayDebugMail($afterpay);

            // Retreive response.
            if (isset($afterpay->order_result->return->statusCode)) {
                switch ($afterpay->order_result->return->statusCode) {
                    case 'A':
                        // Order is Accepted.
                        $transactionId = $afterpay->order_result->return->reservationId;
                        return $transactionId;
                    case 'P':
                        // Order is Pending.
                        /**
                         * @todo: add logic for pending.
                         * Implment riverty_pending_session = true
                         * Implement new session riverty_secure_url = $afterpay->order_result->return->secureLoginUrl
                         */
                        $transactionId = $afterpay->order_result->return->reservationId;
                        return $transactionId;
                    case 'W':
                        if (isset($afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage)) {
                            $this->warning[] = $this->l(
                                $afterpay->order_result->return->riskCheckMessages[0]->customerFacingMessage
                            );
                        }
                        // Order is denied, store it in a database.
                        // @todo: add logic for rejected.
                        return false;
                }
            } else {
                // Check for business errors.
                $customerfacingmessage = '';
                if (1 === $afterpay->order_result->return->resultId) {
                    // Unknown response, store it in a database.
                    $customerfacingmessage = $this->l(
                        'There is a problem with submitting this order to Riverty, please check the following issues: '
                    );

                    if (isset($afterpay->order_result->return->messages)
                        && !is_object($afterpay->order_result->return->messages)
                    ) {
                        foreach ($afterpay->order_result->return->messages as $value) {
                            $customerfacingmessage .= '<li style="list-style: inherit">' .
                            $this->l($value->description) . '</li>';
                            // @todo: specify the validation errors
                        }
                    } elseif (isset($afterpay->order_result->return->failures->failure)) {
                        $customerfacingmessage .= '<li style="list-style: inherit">' .
                        $this->l($afterpay->order_result->return->failures->failure) . '</li>';
                    }
                } elseif (2 === $afterpay->order_result->return->resultId) {
                    // Unknown response, store it in a database.
                    $customerfacingmessage = $this->l(
                        'There is a problem with submitting this order to Riverty, please check the following issues: '
                    );
                    if (! is_object($afterpay->order_result->return->messages)) {
                        $customerfacingmessage .= '<ul>';
                        foreach ($afterpay->order_result->return->messages as $value) {
                            $customerfacingmessage .= '<li style="list-style: inherit">' .
                            $this->l($value->description) . '</li>';
                        }
                        $customerfacingmessage .= '</ul>';
                    }
                } else {
                    // Unknown response, store it in a database.
                    $customerfacingmessage = $this->l(
                        'Unknown response from Riverty. Please contact our customer service'
                    );
                }

                // Set the customerfacingmessage
                $this->warning[] = $customerfacingmessage;

                // Cancel order to make new order possible.
                return false;
            }
        } catch (Exception $e) {
            // The purchase was denied or something went wrong, print the message.
            // translators: %1$s: error message, %2$s: error code.
            $this->module->writeLog($e->getMessage());
            return false;
        }
    }

    /**
    * Function to spit the address in array containing address, house number and house number extension
    *
    * @access public
    * @param  string $address                 Address in one string.
    * @param  bool   $attachSingleExtension   Yes, if a one character extension should be attached to the housenumber.
    * @param  bool   $attachWholeExtension    Yes, if the whole extension should be attached to the housenumber
    * @return array  $address
    **/
    protected function splitAfterpayAddress(
        $address,
        $attachSingleExtension = false,
        $attachWholeExtension = false
    ) {
        $address = is_array($address) ? implode($address, ' ') : $address;
        $ret = [
            'streetname' => '',
            'housenumber' => '',
            'houseNumberAddition' => '',
        ];

        if (preg_match('/^(.+?)([0-9]+)(.*)/', $address, $matches)) {
            $ret['streetname'] = trim($matches[1]);
            $ret['housenumber'] = trim($matches[2]);
            $ret['houseNumberAddition'] = trim($matches[3]);
        }

        // If the streetname is empty after splitting, and the address contains characters, then just use the address.
        if ($ret['streetname'] == '' && Tools::strlen(trim($address)) > 0) {
            $ret['streetname'] = $address;
        }

        if ($attachSingleExtension == true && Tools::strlen($ret['houseNumberAddition']) == 1) {
            $ret['housenumber'] = $ret['housenumber'] . $ret['houseNumberAddition'];
            $ret['houseNumberAddition'] = '';
        }

        if ($attachWholeExtension == true) {
            $ret['housenumber'] = $ret['housenumber'] . $ret['houseNumberAddition'];
            $ret['houseNumberAddition'] = '';
        }

        return [ $ret['streetname'], $ret['housenumber'], $ret['houseNumberAddition'] ];
    }

    /**
     * Function to restore the old cart to a new cart when an order is rejected.
     *
     * @access private
     * @param  array   $oldCartProducts    Array of cart products
     * @param  integer $oldCartCustomerId The ID of the customer
     * @param  string  $oldCartSecureKey  The old secure key
     * @return void
     **/
    private function restoreOldCart($oldCartProducts, $oldCartCustomerId, $oldCartSecureKey)
    {
        $this->context->cart->id_customer = 0;
        $this->context->cart->save();
        $this->context->cart = new Cart();
        $this->context->cart->id_lang = $this->context->language->id;
        $this->context->cart->id_currency = $this->context->currency->id;
        $this->context->cart->add();
        foreach ($oldCartProducts as $product) {
            $this->context->cart->updateQty(
                (int) $product['quantity'],
                (int) $product['id_product'],
                (int) $product['id_product_attribute']
            );
        }
        if ($this->context->cookie->id_guest) {
            $guest = new Guest($this->context->cookie->id_guest);
            $this->context->cart->mobile_theme = $guest->mobile_theme;
        }
        $this->context->cart->id_customer = $oldCartCustomerId;
        $this->context->cart->save();
        if ($this->context->cart->id) {
            $this->context->cookie->id_cart = (int) $this->context->cart->id;
            $this->context->cookie->write();
        }
        $this->context->cart->secure_key = $oldCartSecureKey;
    }
}
