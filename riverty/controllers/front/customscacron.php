<?php
class RivertyCustomSCACronModuleFrontController extends ModuleFrontController
{
    /** 
     * @var bool 
     */
    public $ajax;

    public function initContent()
    {
        /**
         * @todo: Remove the return when the SCA confirms
         * [DP-1040]
         */

        return;

        if (Tools::getValue('token') != Configuration::get('rivertyCronToken')) {
            return;
        }

        /**
         * Fetch the orders from the DB
         * Fetching orders only made at least 12 min ago
         * To make sure customers did not miss the SCA process by any chance
         */
        $sql = 'SELECT * FROM '._DB_PREFIX_.'riverty_sca_info WHERE sca_created_time < NOW() - INTERVAL 12 MINUTE';
        $orders = Db::getInstance()->executeS($sql);

        if (!empty($orders)) {
            foreach ($orders as $order) {
                $rowId = (int)$order['sca_record_id'];
                $orderInfo = unserialize($order['sca_info']);

                /**
                 * order id
                 * order number -reference
                 * connection mode (end point)
                 * API key
                 * payment method
                 */
                $orderId = $orderInfo['orderId'];
                $orderNumber = $orderInfo['orderNumber'];
                $afterpayConnectionMode = $orderInfo['afterpayConnectionMode'];
                $afterpayApiKey = $orderInfo['afterpayApiKey'];
                $order = new Order($orderId);
                $paymentMethod = $order->payment;

                //send the get order request and fetch the request
                $orderResult = $this->module->getSCAOderDetails($paymentMethod, $orderNumber, $afterpayConnectionMode, $afterpayApiKey);
                $orderResultStatus = $orderResult->return->orderDetails->status;

                if ($orderResultStatus == 'Accepted') {
                    $paymentStatus = $this->module->getOrderStateId('Riverty - Authorized');
                    $order->setCurrentState($paymentStatus);
                    
                    //Check if the order should be captured.
                    $useCaptures = Configuration::get('AFTERPAY_USE_CAPTURES');
                    if ($useCaptures == 'yes-autocapture') {
                        $invoiceNumber = $order->invoice_number;
                        $invoice = OrderInvoice::getInvoiceByNumber($invoiceNumber);
                        $invoiceNumberFormatted = $invoice->getInvoiceNumberFormatted($order->id_lang);
                        $captureResult = $this->module->redirectCaptureOrder(
                            $orderId['orderId'],
                            $invoiceNumberFormatted,
                            $afterpayConnectionMode,
                            $afterpayApiKey,
                            $orderInfo['afterpayOrderlines'],
                            $orderInfo['afterpayOrder'],
                            $orderInfo['orderNumber']
                        );
                        if ($captureResult === true) {
                            $paymentStatus = $this->module->getOrderStateId('Riverty - Captured');
                            $order->setCurrentState($paymentStatus);
                        } else {
                            $paymentStatus = $this->module->getOrderStateId('Payment error');
                            $order->setCurrentState($paymentStatus);
                        }
                    }
                } else {
                    $paymentStatus = Configuration::get('PS_OS_ERROR');
                    $order->setCurrentState($paymentStatus);
                }
                /**
                 * We need to delete the temporarily made row
                 * to decarease the perfomrance overheads and
                 * to avoid storage issues
                */
                Db::getInstance()->delete('riverty_sca_info', 'sca_record_id = ' . $rowId);
            }
        }
    }
}
