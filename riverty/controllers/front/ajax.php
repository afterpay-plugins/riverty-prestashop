<?php

class RivertyAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $paymentMethodId = $_POST['paymentMethodId'];
        if($_POST['IBAN'] == ''){
            $iban = $_POST['IBAN1'];
        }
        else {
            $iban = $_POST['IBAN'];
        }
        
        $Afterpay = new \Afterpay\Afterpay();
        $Afterpay->setRest(true);
        $Afterpay->set_ordermanagement('validate_bankaccount');
        
        // Set up the additional information
        $aporder['bankAccount'] = $iban;
        
        // Create the order object for order management (OM)
        $Afterpay->set_order($aporder, 'OM');
        $paymentMethodId = str_replace('afterpay_', '', Tools::strtolower($paymentMethodId));

        // Set up the AfterPay credentials and sent the order
        $modus = $this->getConnectionMode($paymentMethodId);
        $authorisation['apiKey'] = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentMethodId) . '_'. Tools::strtoupper($modus) .'_API_KEY');
        
        $Afterpay->do_request($authorisation, $modus);
        echo json_encode($Afterpay);
    }

    public function getConnectionMode($paymentMethodId)
    {
        $connectionMode = Configuration::get('AFTERPAY_' . Tools::strtoupper($paymentMethodId) . '_TESTMODE');
        switch ($connectionMode) {
            case 'test':
            default:
                $mode = 'test';
                break;
            case 'production':
                $mode = 'production';
                break;
            case 'sandbox':
                $mode = 'sandbox';
                break;
        }
        return $mode;
    }
}
