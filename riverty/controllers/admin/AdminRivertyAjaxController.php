<?php

class AdminRivertyAjaxController extends ModuleAdminController
{
    public function initContent()
    {
        $apiKey = Tools::getValue('apiKeyValue');
        $testMode = Tools::getValue('testModeValue');
        $country = Tools::getValue('country');
        $module = Module::getInstanceByName("riverty");
        $apmResult = $this->getPaymentsCall($apiKey, $testMode, $country);
        echo json_encode($apmResult);
    }

    public function getPaymentsCall($apiKey, $testMode, $country)
    {
        $Afterpay = new \Afterpay\Afterpay();
        $Afterpay->setRest(true);
        $Afterpay->set_ordermanagement('available_payment_methods');
        $module = Module::getInstanceByName("riverty");
        $currency =[];
        $currency[]['DE']['AT']['BE']['FI']['NL'] ='EUR';
        $currency['DK'] ='DKK';
        $currency['NO'] ='NOK';
        $currency['SE'] ='SEK';
        $currency['CH'] ='CHF';
        $order = [
            'order' => [
                'totalGrossAmount' => 1000,
                'totalNetAmount' => 1000,
                'currency' => $currency[$country]
            ],
            'country'  => $country,
            'conversationLanguage' => "EN",
            'additionalData' => [
                'pluginProvider' => $module->author,
                'pluginVersion' => $module->version,
                'shopUrl' => Tools::getHttpHost(true).Configuration::get('PS_BASE_URL'),
                'shopPlatform' => 'Prestashop',
                'shopPlatformVersion' => Configuration::get('PS_INSTALL_VERSION')
                ],
        ];
        $Afterpay->set_order($order, 'OM');

        // Get connection mode.
        $afterpayConnectionMode = $this->getConnectionMode($testMode);

        // Get authorization key.
        $authorisation = [];
        $authorisation['apiKey'] = $apiKey;

        $Afterpay->do_request($authorisation, $afterpayConnectionMode);

        if (isset($Afterpay->order_result->return) && $Afterpay->order_result->return->resultId == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getConnectionMode($connectionMode)
    {
        switch ($connectionMode) {
            case 'test':
            default:
                $mode = 'test';
                break;
            case 'production':
                $mode = 'production';
                break;
            case 'sandbox':
                $mode = 'sandbox';
                break;
        }
        return $mode;
    }
}
