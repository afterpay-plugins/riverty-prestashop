# Riverty Payment Module for Prestashop #

The official Prestashop module for the Riverty payment method. Riverty is the new AfterPay. This module offers a direct connection with the Riverty Buy Now Pay Later (BNPL) payment services in Germany, Austria, Switzerland, The Netherlands, Belgium, Norway, Denmark, Sweden and Finland.

## Installation ##

The installation of the Prestashop Plugin can be done through the Prestashop Module Manager, which can be found under Improve > Modules > Module manager. In the Module manager, you have to upload the module by clicking on the button “Upload a module”. A screen will appear in which the ZIP file can be uploaded.

The module will automatically be installed after uploading, and when the installation is successful it can be configured by clicking on “Configure”.

This module uses a static version of the Riverty PHP SDK and the GuzzleHttp class. Because Prestashop already contains an older version of the GuzzleHttp class, a static copy is used to avoid conflicts.

## Release notes ##

**1.9.0 (2024-03-06)**

* Bug fixes

**1.8.0 (2024-01-18)**

* We have added support to Prestashop version 8.1.1

**1.7.0 (2023-08-11)**

* We have improved the user experience of Pay in 3
* We have improved the merchant experience by adding more information to the plugin configuration and also by adding API key validation.
* We have done several small technical updates to improve the quality of the plugin.

**1.6.0 (2023-02-22)**

* We have added Pay in 3, a new payment method for the Netherlands. Contact your Riverty account manager for more information.
* We have updated and tested the compatibility with the latest version of Prestashop (1.7.8.8).
* We have updated the required fields, like phone number and date of birth, and improved the validation messages.
* We have updated the Riverty PHP SDK to version 3.9.0

**1.5.0 (2022-11-14)**

* We have implemented the payment method 'fixed instalment' for DACH (Germany and Austria)
* We have added profile tracking for Germany, Austria and Switzerland.
* We have fixed an issue with partial refunds.
* The backend of the module is changed from AfterPay to Riverty.
* The frontend of the module is changed from AfterPay to Riverty.
* We have fixed an issue with validation rules on the payment method Direct Debit.
* We have updated and removed the translation syntax for the admin panel.
* We have updated and tested the compatibility with the latest version of Prestashop (1.7.8.7).
* We have updated the Riverty PHP SDK to version 3.7.0.
* We have added the country to the available payment methods call, to retrieve legal information from the Riverty API.
* We now use the legal info that comes from the Riverty API.

**1.4.0 (2022-09-05)**

* Updated bank account input field with masking and automatic validation.
* Updated PHP Library to version 3.6.0
* Implemented available payment methods method from API.
* Provide plugin and platform information in requests.
* Added extra information to order succesful / thank you page.
* Added introduction text to checkout form fields.
* Small visual improvement.

**1.3.1 (2022-07-20)**

* Checked compatibility with latest version of Prestashop (1.7.8.5).
* Updated to version 3.5.0 of the AfterPay PHP Library.
* Updated to version 3.5.1 of the AfterPay PHP Library.
* Updated to version 3.5.2 of the AfterPay PHP Library.
* Added feature to add merchant specific terms and conditions.
* Removed the gender option from the checkout.
* Changed the order of the payment methods.
* Changed the titles of the payment methods.
* Added payment methods Digital Invoice for B2B in the Netherlands.
* Added payment methods Direct Debit in the Netherlands.
* Changed styling of the terms and conditions in the checkout.
* Added box with unique selling points to the checkout.
* Added date of birth for all payment methods in Germany, Netherlands, Austria, Switzerland and Denmark.
* Added confirmation message to backend configuration.
* Added functionality to open terms and conditions in a modal window.

**1.3.0 (2021-10-07)**

* Updated the AfterPay logo.
* Removed BIC fields from Direct Debit and Installment payment methods.
* Updated AfterPay PHP Library to version 3.3.0.
* Adding plugin data fields in the authorize call.

**1.2.0 (2021-07-27)**

* Added Digital Invoice for Austria.
* Added Direct Debit for Austria.
* Added Digital Invoice for Switzerland.
* Added Digital Invoice for Norway.
* Added Digital Invoice for Sweden.
* Added Digital Invoice for Denmark.
* Added Digital Invoice for Finland.
* Added order management operations - refunds.
* Update AfterPay PHP Library to version 3.2.0
* Fixed issue with streetnames with or without housenumbers.
* Removed the merchant ID from API calls.
* Added capture based on status and changed default mode.

**1.1.0 (2021-04-30)**

* Updated AfterPay PHP Library to version 3.1.0.
* Multiple small fixes (rounding, customer experience)

**1.0.0 (2021-03-10)**

* Setting up the base of the Prestashop payment module.
* Integrated AfterPay PHP Library.
* Created boilerplate payment method.
* Added digital invoice for the Netherlands and Germany.
* Added direct debit for Netherlands and Germany.
* Added Digital Invoice B2B for the Netherlands.
* Added order management operations - captures.
* Added order management operations - refunds.
* Updated PHP Library to version 3.0.0.
* Validated prestashop module with Prestashop Validator Tool, and processed feedback.
