<?php
/**
 * Prestaworks AB
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://license.prestaworks.se/license.html
 *
 * @author    Prestaworks AB <info@prestaworks.se>
 * @copyright Copyright Prestaworks AB (https://www.prestaworks.se/)
 * @license   http://license.prestaworks.se/license.html
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Update the AfterPay brand name in the DB
 * Add a new table to the DB to hold SCA info temporary
 * @return bool
 */
function upgrade_module_1_7_0()
{
    $configNames = [
        'AFTERPAY_AT_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_AT_DIRECTDEBIT_TESTMODE',
        'AFTERPAY_AT_INSTALLMENTS_TESTMODE',
        'AFTERPAY_BE_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_CH_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_DE_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_DE_DIRECTDEBIT_TESTMODE',
        'AFTERPAY_DE_INSTALLMENTS_TESTMODE',
        'AFTERPAY_NL_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_NL_PAYIN3_TESTMODE',
        'AFTERPAY_NL_DIRECTDEBIT_TESTMODE',
        'AFTERPAY_NL_BUSINESS_TESTMODE',
        'AFTERPAY_NO_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_SE_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_FI_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_DK_DIGITALINVOICE_TESTMODE',
        'AFTERPAY_NO_DIGITALINVOICE_TESTMODE',
    ];
    $updateQuery = [];

    foreach ($configNames as $name) {
        $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `value` = ( CASE 
        WHEN `value` = "yes" then "test"
        WHEN `value` = "no" then "production"
        WHEN `value` = "yes-sandbox" then "sandbox"
        ELSE `value`
        END )
        WHERE `name` = "'.$name.'";';
    }
    
    foreach ($updateQuery as $sql) {
        if (Db::getInstance()->execute($sql) == false) {
            return false;
        }
    }

    foreach ($configNames as $name) {
        $connectionMode = Db::getInstance()->executeS('SELECT `value` from `' . _DB_PREFIX_ . 'configuration` WHERE `name` = "'.$name.'";');
        if($connectionMode[0]['value']){
            switch($connectionMode[0]['value']){
                case 'test':
                    $updateQuery = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `name` = "'.str_replace("TESTMODE", "TEST_API_KEY", $name).'"
                    WHERE `name` = "'.str_replace("TESTMODE", "API_KEY", $name).'";';
                    break;
                case 'production':
                    $updateQuery = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `name` = "'.str_replace("TESTMODE", "PRODUCTION_API_KEY", $name).'"
                    WHERE `name` = "'.str_replace("TESTMODE", "API_KEY", $name).'";';
                    break;   
                case "sandbox":
                    $updateQuery = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `name` = "'.str_replace("TESTMODE", "SANDBOX_API_KEY", $name).'"
                    WHERE `name` = "'.str_replace("TESTMODE", "API_KEY", $name).'";';
                    break;
                default:
                    return false;
            }
            if (Db::getInstance()->execute($updateQuery) == false) {
                return false;
            }
        }
        
    }

    $merchantFields =[
        'AFTERPAY_AT_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_AT_DIRECTDEBIT_MERCHANT_ID',
        'AFTERPAY_AT_INSTALLMENTS_MERCHANT_ID',
        'AFTERPAY_BE_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_CH_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_DE_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_DE_DIRECTDEBIT_MERCHANT_ID',
        'AFTERPAY_DE_INSTALLMENTS_MERCHANT_ID',
        'AFTERPAY_NL_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_NL_PAYIN3_MERCHANT_ID',
        'AFTERPAY_NL_DIRECTDEBIT_MERCHANT_ID',
        'AFTERPAY_NL_BUSINESS_MERCHANT_ID',
        'AFTERPAY_NO_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_SE_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_FI_DIGITALINVOICE_MERCHANT_ID',
        'AFTERPAY_DK_DIGITALINVOICE_MERCHANT_ID',
    ];

    $updateMerchantFields = [];

    foreach ($merchantFields as $field) {
        $updateMerchantFields[] = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `value` = "NULL" WHERE `name` = "'.$field.'";';
    }
    
    foreach ($updateMerchantFields as $sql) {
        if (Db::getInstance()->execute($sql) == false) {
            return false;
        }
    }
    
    return true;
}
