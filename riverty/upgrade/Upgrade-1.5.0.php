<?php
/**
 * Prestaworks AB
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://license.prestaworks.se/license.html
 *
 * @author    Prestaworks AB <info@prestaworks.se>
 * @copyright Copyright Prestaworks AB (https://www.prestaworks.se/)
 * @license   http://license.prestaworks.se/license.html
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Update the AfterPay brand name in the DB
 * @return bool
 */
function upgrade_module_1_5_0()
{
    $updateQuery = [];
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Awaiting payment" WHERE `name` = "AfterPay - Awaiting payment";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Authorized" WHERE `name` = "AfterPay - Authorized";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Rejected" WHERE `name` = "AfterPay - Rejected";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Validation issue" WHERE `name` = "AfterPay - Validation issue";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Captured" WHERE `name` = "AfterPay - Captured";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Partially refunded" WHERE `name` = "AfterPay - Partially refunded";';
    $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'order_state_lang` SET `name` = "Riverty - Fully refunded" WHERE `name` = "AfterPay - Fully refunded";';

    foreach ($updateQuery as $sql) {
        if (Db::getInstance()->execute($sql) == false) {
            return false;
        }
    }

    return true;
}
