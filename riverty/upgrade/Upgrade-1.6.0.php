<?php
/**
 * Prestaworks AB
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://license.prestaworks.se/license.html
 *
 * @author    Prestaworks AB <info@prestaworks.se>
 * @copyright Copyright Prestaworks AB (https://www.prestaworks.se/)
 * @license   http://license.prestaworks.se/license.html
 */
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Update the AfterPay brand name in the DB
 * @return bool
 */
function upgrade_module_1_6_0()
{
    $configNames = [
        'AFTERPAY_AT_DIGITALINVOICE_SHOW_DOB',
        'AFTERPAY_AT_DIRECTDEBIT_SHOW_DOB',
        'AFTERPAY_AT_INSTALLMENTS_SHOW_DOB',
        'AFTERPAY_BE_DIGITALINVOICE_SHOW_DOB',
        'AFTERPAY_DE_DIGITALINVOICE_SHOW_DOB',
        'AFTERPAY_DE_DIRECTDEBIT_SHOW_DOB',
        'AFTERPAY_DE_INSTALLMENTS_SHOW_DOB',
        'AFTERPAY_NL_DIGITALINVOICE_SHOW_DOB',
        'AFTERPAY_NL_PAYIN3_SHOW_DOB',
        'AFTERPAY_NL_DIRECTDEBIT_SHOW_DOB',
        'AFTERPAY_NL_BUSINESS_SHOW_DOB',
        'AFTERPAY_CH_DIGITALINVOICE_SHOW_DOB',
        'AFTERPAY_DK_DIGITALINVOICE_SHOW_DOB',
    ];
    $updateQuery = [];

    foreach ($configNames as $name) {
        $updateQuery[] = 'UPDATE `' . _DB_PREFIX_ . 'configuration` SET `value` = "1" WHERE `name` = "'.$name.'";';
    }
    
    foreach ($updateQuery as $sql) {
        if (Db::getInstance()->execute($sql) == false) {
            return false;
        }
    }

    $tab = new Tab();
    $tab->active = 1;
    $tab->class_name = 'AdminRivertyAjax';
    $tab->name = array();
    foreach (Language::getLanguages(true) as $lang) {
        $tab->name[$lang['id_lang']] = 'AdminRivertyAjax';
    }
    $tab->id_parent = 0;
    $tab->module = "riverty";
    $tab->add();

    return true;
}
